CS587: Game Engine Design Complete Source Code
==============================================

Code Produced By: Zachary Smith (Spring 2014)
---------------------------------------------

------------------------------------------------

# Style Guide

------------------------------------------------


Naming Conventions
==================

### UpperCamelCase
-	Classes
-	Structs
-	Namespaces
-	Template Parameters (Types)

### lowerCamelCase
-	Functions
-	Stack Variables
-	Class Members

### UPPER\_SNAKE\_CASE
-	Constants
-	Macros
-	Template Parameters (integers)
-	Enum Values

### \_Underscore \_Prefixed
-	Protected Class Members
-	Private Class Members

### Sense
-	Variable names should be nouns
-	Function names should be verbs with the exception of accessors, which can be nouns.
	- ie: member variable position would have a mutator `setPosition()` and accessor `position()`
-	Type prefixes (hungarian Notation) are not allowed
-	Boolean variables and functions should use language that indicates they are predicates
	-	names do not containt the word not


### File Names
-	Use CamelCase
	-	Should mirror name of class defined with (if there is a class definition)
-	C++ files should use the `.cpp` extension
-	Header files should use the `.hpp' extension
-	Avoid numbers (number literals)

Indentation, Spacing, Braces
============================

### Indented Items
-	Control statement bodies: `for, if, while, switch,` etc.
-	Class and Function bodies
-	Statements coninues from previous line
-	Body of block used to limit scope of object

### Non-Indented Items
-	Contents of namespaces
-	Case labels;
-	Access control specifiers

### Braces
-	Braces are always the only thing on a line.
	- Exception: Name
-	Mandatory, even if block has only a single line of code in it

### Spaces are Present
-	Around all binary operators
-	Between keyword and parentheses
-	Between keyword/identifies and braces
-	After commas and semicolons that do not end the line
-	Between template keyword and template arugment list

### Spaces are Not Present
-	Before semicolons and commas
-	on the inner side of parentheses
-	Between a function namd and its argument list
-	Between unary operators and their operands
-	inside an empty argument list
-	Around the scopre operator

### Aligning Statements and Expressions
Align statements and expressions that form logical groups.

		enum
		{
		    Count       = 8, // Aligned at =
		    ButtonCount = 32,
		    AxisCount   = 8
		};

### Member initializer lists and base class lists
Each member or class goes on a separate line and is preceded by the appropriate punctuation and a space. 

### Parameter/Argument Lists
For functions with large numbers of arguments, the arguments can be spread over multiple lines. This is done by putting each argument on a separate line, indenting each argument to be at the same level as the first argument. The function body and braces is still indented as normal.

**Example**

		int Function(int index,
					 std::string& someValue,
					 bool isThisAnotherValue)
		{
			// ... function body
		}

		int x = Function(5, "hello", true);	// Arguments on the same line
		x = Function(5,
					 "hello",
					 true); 				// Each argument on a separate line

### New Lines
Up to one empty line can be used to separate logical sections of code. 

File Layout
===========

Each code file should contain things in this order:

1.	Block comment describing file contents
2.	Open of include gaurd (only in headers)
3.	System header incldues, e.g. `#include <header>`
4.	Internal header includes, e.g. `#include "header.hpp"`
5.	Code itself. Order of .cpp definitions shoud mirror .hpp definitions.
7.	Close include guard

###	Include Guard
The include guard should appear as follows:

		#ifndef FILE_NAME_IN_ALL_UPPERCASE_HPP
		#define FILE_NAME_IN_ALL_UPPERCASE_HPP

			// ... code body

		#endif // FILE_NAME_IN_ALL_UPPERCASE_HPP

### Code Itself

Not Operator
============

### Inverting boolean value
Do not use the `!` to invert a boolean value. Compare with explicit false.

### `nullptr`
Avoid using the not operator on a point. Compare to `nullptr` explicitly

		Object* obj;
		if (!obj) // avoid this
		{
			doWork();
		}

		Object* obj;
		if (obj == nullptr) // this can be read as a sentence
		{
			doWork();
		}


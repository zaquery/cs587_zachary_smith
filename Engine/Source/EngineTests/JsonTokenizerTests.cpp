#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Json.h"

#include "JsonTypesToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
    TEST_CLASS(JsonTokenizerTest)
    {
    public:
        TEST_METHOD(JsonTokenizer_primitives)
        {
            JsonTokenizer tokenizer;
            std::string str = "123 -123 1.45 true false \"test string\"";
            std::stringstream stream(str);

            tokenizer.startReading(&stream);

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("123"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("-123"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("1.45"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("true"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("false"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("\"test string\""), tokenizer.getNextValue());

            Assert::IsFalse(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::DOCUMENT_END, tokenizer.peek());
        }

        TEST_METHOD(JsonTokenizer_object)
        {
            JsonTokenizer tokenizer;
            std::string str = "{\"type\":true,\"value\":123}";
            std::stringstream stream(str);
            tokenizer.startReading(&stream);

            tokenizer.startObject();

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::PROPERTY, tokenizer.peek());
            Assert::AreEqual(std::string("type"), tokenizer.getNextName());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("true"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::PROPERTY, tokenizer.peek());
            Assert::AreEqual(std::string("value"), tokenizer.getNextName());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("123"), tokenizer.getNextValue());

            Assert::IsFalse(tokenizer.hasNext());
            tokenizer.endObject();
        }
        TEST_METHOD(JsonTokenizer_array_endcases)
        {
            JsonTokenizer tokenizer;
            std::string str = "[ 123, 456]";
            std::stringstream stream(str);
            tokenizer.startReading(&stream);
            
            tokenizer.startArray();

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(std::string("123"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(std::string("456"), tokenizer.getNextValue());

            Assert::IsFalse(tokenizer.hasNext());
            tokenizer.endArray();
        }

        TEST_METHOD(JsonTokenizer_combinedTest)
        {
            JsonTokenizer tokenizer;
            std::string str = "{ \"value0\":\t\"test value\", \"test_array\":[ 0, 1, 2, 3, \"Another String\" ] }";
            std::stringstream stream(str);
            
            tokenizer.startReading(&stream);
            
            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::OBJECT_BEGIN, tokenizer.peek());
            tokenizer.startObject();

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::PROPERTY, tokenizer.peek());
            Assert::AreEqual(std::string("value0"), tokenizer.getNextName());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("\"test value\""), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::PROPERTY, tokenizer.peek());
            Assert::AreEqual(std::string("test_array"), tokenizer.getNextName());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::ARRAY_BEGIN, tokenizer.peek());
            tokenizer.startArray();

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("0"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("1"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("2"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("3"), tokenizer.getNextValue());

            Assert::IsTrue(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::VALUE, tokenizer.peek());
            Assert::AreEqual(std::string("\"Another String\""), tokenizer.getNextValue());

            Assert::IsFalse(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::ARRAY_END, tokenizer.peek());
            tokenizer.endArray();

            Assert::IsFalse(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::OBJECT_END, tokenizer.peek());
            tokenizer.endObject();

            Assert::IsFalse(tokenizer.hasNext());
            Assert::AreEqual(JsonTokenizer::TokenType::DOCUMENT_END, tokenizer.peek());
        }
    };
}
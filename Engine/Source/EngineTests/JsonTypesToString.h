#include "CppUnitTest.h"

#include "Engine/Json.h"

using namespace Engine;

namespace Microsoft
{
    namespace VisualStudio
    {
        namespace CppUnitTestFramework
        {
            template<> static std::wstring ToString<JsonElement::Type>(const JsonElement::Type& value) 
            { 
                switch (value)
                {
                case JsonElement::Type::PRIMITIVE:
                    return std::wstring(L"PRIMITIVE");
                case JsonElement::Type::ARRAY:
                    return std::wstring(L"ARRAY");
                case JsonElement::Type::OBJECT:
                    return std::wstring(L"OBJECT");
                default:
                    return std::wstring(L"INVALID TYPE: THIS SHOULD NOT HAPPEN");
                }
            }

            template<> static std::wstring ToString<JsonTokenizer::TokenType>(const JsonTokenizer::TokenType& value)
            {
                switch (value)
                {
                case JsonTokenizer::TokenType::ARRAY_BEGIN:
                    return std::wstring(L"ARRAY_BEGIN");
                case JsonTokenizer::TokenType::ARRAY_END:
                    return std::wstring(L"ARRAY_END");
                case JsonTokenizer::TokenType::OBJECT_BEGIN:
                    return std::wstring(L"OBJECT_BEGIN");
                case JsonTokenizer::TokenType::OBJECT_END:
                    return std::wstring(L"OBJECT_END");
                case JsonTokenizer::TokenType::DOCUMENT_END:
                    return std::wstring(L"DOCUMENT_END");
                case JsonTokenizer::TokenType::VALUE:
                    return std::wstring(L"PRIMITIVE");
                case JsonTokenizer::TokenType::NULL_VALUE:
                    return std::wstring(L"NULL_VALUE");
                case JsonTokenizer::TokenType::PROPERTY:
                    return std::wstring(L"PROPERTY");
                }
            }
        }
    }
}
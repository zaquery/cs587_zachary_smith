#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Json.h"

#include "JsonTypesToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
    TEST_CLASS(JsonArrayTests)
    {
    public:
        
        TEST_METHOD(JsonArray_type)
        {
            JsonArray array;
            Assert::AreEqual(JsonElement::Type::ARRAY, array.type());
        }

        TEST_METHOD(JsonArray_constructor)
        {
            JsonArray array;
            Assert::AreEqual(0U, array.count());
        }

        TEST_METHOD(JsonArray_get_append)
        {
            JsonArray array;
            array.append(new JsonPrimitive("1"));
            array.append(new JsonPrimitive("2"));
            array.append(new JsonPrimitive("3"));
            array.append(new JsonPrimitive("4"));
            array.append(new JsonPrimitive("5"));
            
            Assert::AreEqual(5U, array.count());
            Assert::AreEqual(1, array.get(0)->asPrimitive()->asInt());
            Assert::AreEqual(2, array.get(1)->asPrimitive()->asInt());
            Assert::AreEqual(3, array.get(2)->asPrimitive()->asInt());
            Assert::AreEqual(4, array.get(3)->asPrimitive()->asInt());
            Assert::AreEqual(5, array.get(4)->asPrimitive()->asInt());
        }
    };
}
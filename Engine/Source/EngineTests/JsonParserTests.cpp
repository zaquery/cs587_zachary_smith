#include "stdafx.h"
#include "CppUnitTest.h"

#include <string>
#include <sstream>
#include <direct.h>

#include "Engine/Json.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
	TEST_CLASS(JsonParserTests)
	{
	public:
		
        TEST_METHOD(JsonParser_parse_tokenizer)
		{
            JsonTokenizer tokenizer;
            std::stringstream stream(std::string("1234"));
            tokenizer.startReading(&stream);

            JsonElement* element = JsonParser::parse(tokenizer);
            Assert::IsNotNull(element);
            Assert::IsTrue(element->isPrimitive());
            Assert::AreEqual(1234, element->asPrimitive()->asInt());
            delete element;
		}

        TEST_METHOD(JsonParser_parse_string)
        {
            JsonElement* element = JsonParser::parse("[ true, false, 123]");
            Assert::IsNotNull(element);
            Assert::IsTrue(element->isArray());
            Assert::IsTrue(element->asArray()->get(0)->asPrimitive()->asBool());
            Assert::IsFalse(element->asArray()->get(1)->asPrimitive()->asBool());
            Assert::AreEqual(123, element->asArray()->get(2)->asPrimitive()->asInt());
            delete element;
        }

        TEST_METHOD(JsonParser_parse_object)
        {
            JsonElement* element = JsonParser::parse("{ \"value\": 123, \"type\":634 }");
            Assert::IsNotNull(element);
            Assert::IsTrue(element->isObject());
            Assert::AreEqual(123, element->asObject()->find("value")->asPrimitive()->asInt());
            Assert::AreEqual(634, element->asObject()->find("type")->asPrimitive()->asInt());
            delete element;
        }

        TEST_METHOD(JsonParser_parseFromFile)
        {
            JsonElement* element = JsonParser::parseFromFile("../../../Source/EngineTests/SimpleJsonTest.json");
            Assert::IsNotNull(element);
            Assert::IsTrue(element->isObject());

            JsonObject* object = element->asObject();
            Assert::IsTrue(object->has("type"));
            Assert::IsTrue(object->has("ids"));
            Assert::AreEqual(std::string("player"), object->find("type")->asPrimitive()->asString());
            
            JsonArray* array = object->find("ids")->asArray();
            Assert::AreEqual(4U, array->count());
            Assert::AreEqual(1, array->get(0)->asPrimitive()->asInt());
            Assert::AreEqual(2, array->get(1)->asPrimitive()->asInt());
            Assert::AreEqual(3, array->get(2)->asPrimitive()->asInt());
            Assert::AreEqual(4, array->get(3)->asPrimitive()->asInt());
            
            delete element;
        }

	};
}
#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Json.h"

#include "JsonTypesToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
    TEST_CLASS(JsonElementTests)
    {
    public:
        TEST_METHOD(JsonElement_as_is_Primitive)
        {
            JsonElement* element = new JsonPrimitive("145");
            Assert::IsTrue(element->isPrimitive());
            Assert::AreEqual(145, element->asPrimitive()->asInt());
            delete element;
        }

        TEST_METHOD(JsonElement_as_is_Array)
        {
            JsonElement* element = new JsonArray();
            Assert::IsTrue(element->isArray());
            element->asArray()->append(new JsonPrimitive("145"));
            Assert::AreEqual(145, element->asArray()->get(0)->asPrimitive()->asInt());
        }

        TEST_METHOD(JsonElement_as_is_Object)
        {
            JsonElement* element = new JsonObject();
            Assert::IsTrue(element->isObject());
            element->asObject()->add("testProperty", new JsonPrimitive("145"));
            Assert::AreEqual(145, element->asObject()->find("testProperty")->asPrimitive()->asInt());
        }
    };
}
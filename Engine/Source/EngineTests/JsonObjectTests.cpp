#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Json.h"

#include "JsonTypesToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
    TEST_CLASS(JsonObjectTests)
    {
    public:
        
        TEST_METHOD(JsonObject_type)
        {
            JsonObject object;
            Assert::AreEqual(JsonElement::Type::OBJECT, object.type());
        }

        TEST_METHOD(JsonObject_has)
        {
            JsonObject object;
            Assert::IsFalse(object.has("test"));
            object.add("test", new JsonPrimitive("123"));
            Assert::IsTrue(object.has("test"));
            Assert::AreEqual(123, object.find("test")->asPrimitive()->asInt());
        }

        TEST_METHOD(JsonObject_find_add)
        {
            JsonObject object;
            Assert::IsFalse(object.has("test1"));
            Assert::IsNull(object.find("test1"));
            object.add("test1", new JsonPrimitive("123"));
            object.add("test2", new JsonPrimitive("123"));
            object.add("test3", new JsonPrimitive("123"));
            Assert::IsTrue(object.has("test1"));
            Assert::AreEqual(123, object.find("test1")->asPrimitive()->asInt());
            Assert::IsNull(object.find("anotherTest"));
        }

    };
}
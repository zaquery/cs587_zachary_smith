#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Collections/DynamicArray.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{        
TEST_CLASS(DynamicArrayTests)
{
public:
        
    TEST_METHOD(DynamicArray_constructor_default)
    {
        DynamicArray<uint> arr = DynamicArray<uint>();
        Assert::IsTrue(arr.isEmpty());
        Assert::AreEqual(0U, arr.size());
        Assert::IsTrue(arr.capacity() > 0);
    }

    TEST_METHOD(DynamicArray_constructor_int)
    {
        DynamicArray<uint> arr(20);
        Assert::IsTrue(arr.isEmpty());
        Assert::AreEqual(0U, arr.size());
        Assert::AreEqual(20U, arr.capacity());
    }

    TEST_METHOD(DynamicArray_constructor_copy)
    {
        DynamicArray<uint> arr(20);
        for (uint i = 0; i < 20; ++i)
        {
            arr.pushBack(i);
        }

        DynamicArray<uint> copy_arr(arr);
        Assert::AreEqual(20U, copy_arr.size());
        Assert::IsTrue(copy_arr.capacity() >= 20);

        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(i, copy_arr[i]);
        }

        copy_arr[0] = 40;
        Assert::AreEqual(40U, copy_arr[0]);
        Assert::AreEqual(0U, arr[0]);
    }

    TEST_METHOD(DynamicArray_constructor_move)
    {
        DynamicArray<uint> arr(20);
        for (uint i = 0; i < 20; ++i)
        {
            arr.pushBack(i);
        }

        DynamicArray<uint> move_arr(std::move(arr));
        arr.~DynamicArray();

        Assert::AreEqual(20U, move_arr.size());
        Assert::IsTrue(move_arr.capacity() >= 20);

        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(i, move_arr[i]);
        }
    }
        
    TEST_METHOD(DynamicArray_operator_assign_copy)
    {
        DynamicArray<uint> arr(20);
        DynamicArray<uint> copy_arr;
        for (uint i = 0; i < 20; ++i)
        {
            arr.pushBack(i);
        }

        copy_arr = arr;
        Assert::AreEqual(20U, copy_arr.size());
        Assert::IsTrue(copy_arr.capacity() >= 20);

        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(i, copy_arr[i]);
        }

        copy_arr[0] = 40;
        Assert::AreEqual(40U, copy_arr[0]);
        Assert::AreEqual(0U, arr[0]);
    }

    TEST_METHOD(DynamicArray_operator_assign_move)
    {
        DynamicArray<uint> arr(20);
        DynamicArray<uint> move_arr;
        for (uint i = 0; i < 20; ++i)
        {
            arr.pushBack(i);
        }

        move_arr = std::move(arr);
        arr.~DynamicArray();

        Assert::AreEqual(20U, move_arr.size());
        Assert::IsTrue(move_arr.capacity() >= 20);

        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(i, move_arr[i]);
        }
    }

    TEST_METHOD(DynamicArray_operator_index)
    {
        DynamicArray<uint> arr(20);
        arr.resize(20);
        for (uint i = 0; i < 20; ++i)
        {
            arr[i] = i;
        }

        for (uint i = 0; i < 20; ++i)
        {
            uint j = arr[i];
            Assert::AreEqual(i, j);
        }
    }

    TEST_METHOD(DynamicArray_operator_index_const)
    {
        DynamicArray<uint> arr(20);

        arr.resize(20);
        for (uint i = 0; i < 20; ++i)
        {
            arr[i] = i;
        }

        const DynamicArray<uint> copy_arr = DynamicArray<uint>(arr);

        for (uint i = 0; i < 20; ++i)
        {
            uint j = copy_arr[i];
            Assert::AreEqual(i, j);
        }
    }

    TEST_METHOD(DynamicArray_back)
    {
        DynamicArray<int> arr(20);
        arr.resize(15);
        for (uint i = 0; i < 15; ++i)
        {
            arr[i] = i;
        }
        Assert::AreEqual(14, arr.back());
    }

    TEST_METHOD(DynamicArray_popBack)
    {
        DynamicArray<uint> arr(20);
        arr.resize(20);
        for (uint i = 0; i < 20; ++i)
        {
            arr[i] = i;
        }

        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(19 - i, arr.back());
            arr.popBack();

            Assert::AreEqual(19 - i, arr.size());
        }
        Assert::IsTrue(arr.isEmpty());
    }

    TEST_METHOD(DynamicArray_pushBack)
    {
        DynamicArray<uint> arr;
            
        for (uint i = 0; i < 100; ++i)
        {
            arr.pushBack(i);
        }

        Assert::IsFalse(arr.isEmpty());
        Assert::AreEqual(100U, arr.size());
        Assert::IsTrue(arr.capacity() >= 20U);
        Assert::AreEqual(99U, arr.back());
            
            
        for (uint i = 0; i < 100; i++)
        {
            Assert::AreEqual(i, arr[i]);
        }
    }

    TEST_METHOD(DynamicArray_getFront)
    {
        Assert::Fail();
    }

    TEST_METHOD(DynamicArray_getFront_const)
    {
        Assert::Fail();
    }

    TEST_METHOD(DynamicArray_popFront)
    {
        Assert::Fail();
    }

    TEST_METHOD(DynamicArray_pushFront)
    {
        Assert::Fail();
    }

    TEST_METHOD(DynamicArray_getData)
    {
        DynamicArray<uint> arr;
        for (uint i = 0; i < 10; ++i)
        {
            arr.pushBack(i);
        }
            
        uint* data = arr.getData();
        Assert::IsFalse(nullptr == data);
        for (uint i = 0; i < arr.size(); ++i)
        {
            Assert::AreEqual(i, data[i]);
        }
    }

    TEST_METHOD(DynamicArray_contains)
    {
        Assert::Fail();
    }

    TEST_METHOD(DynamicArray_find)
    {
        Assert::Fail();
    }

    //TEST_METHOD(DynamicArray_remove_at)
    //{
    //    Assert::Fail();
    //}

    TEST_METHOD(DynamicArray_fill)
    {
        DynamicArray<uint> arr;
        arr.resize(20);
        arr.fill(20U);
        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(20U, arr[i]);
        }
    }

    TEST_METHOD(DynamicArray_isEmpty)
    {
        DynamicArray<uint> arr;
        Assert::IsTrue(arr.isEmpty());
        arr.pushBack(3U);
        Assert::IsFalse(arr.isEmpty());
    }

    TEST_METHOD(DynamicArray_size)
    {
        DynamicArray<uint> arr(10);
        uint value = 30;
        Assert::AreEqual(0U, arr.size());
        arr.pushBack(1U);
        Assert::AreEqual(1U, arr.size());
    }

    TEST_METHOD(DynamicArray_capacity)
    {
        DynamicArray<uint> arr(10);
        Assert::AreEqual(10U, arr.capacity());

        arr.reserve(40);
        Assert::AreEqual(40U, arr.capacity());
    }

    TEST_METHOD(DynamicArray_resize)
    {
        DynamicArray<uint> arr;
        arr.resize(20);
        for (uint i = 0; i < 20; ++i)
        {
            arr[i] = i;
        }

        Assert::AreEqual(20U, arr.size());
        Assert::IsTrue(arr.capacity() >= 20U);
        for (uint i = 0; i < 20; ++i)
        {
            Assert::AreEqual(i, arr.getData()[i]);
        }
    }

    TEST_METHOD(DynamicArray_reserve)
    {
        DynamicArray<uint> arr;
        for (uint i = 0; i < 10; ++i)
        {
            arr.pushBack(i);
        }
        arr.reserve(30);
        Assert::AreEqual(30U, arr.capacity());  
        Assert::AreEqual(10U, arr.size());
    }
};
}
#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Util.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
	TEST_CLASS(UtilTests)
	{
	public:
		
		TEST_METHOD(Util_contains)
		{
            std::string testStr("these are test characters \n\t,");
            Assert::IsTrue(Util::contains(testStr, '\n'));
            Assert::IsTrue(Util::contains(testStr, '\t'));
            Assert::IsTrue(Util::contains(testStr, ','));
            Assert::IsTrue(Util::contains(testStr, 'c'));
            Assert::IsFalse(Util::contains(testStr, 'z'));
            Assert::IsFalse(Util::contains(testStr, 'y'));
            Assert::IsFalse(Util::contains(testStr, 'x'));
		}

	};
}
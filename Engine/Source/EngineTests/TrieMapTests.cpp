#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Collections/TrieMap.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
    TEST_CLASS(TrieMapTests)
    {
    public:

        TEST_METHOD(TrieMap_constructor_default)
        {
            TrieMap<int> map;
            Assert::IsFalse(map.contains("world"));
            Assert::IsTrue(map.find("key") == nullptr);
        }

        TEST_METHOD(TrieMap_constructor_copy)
        {
            TrieMap<int> map;
            map.try_set("value", 6);

            TrieMap<int> copy_map(map);
            map.try_set("another_value", 10);

            Assert::IsTrue(copy_map.contains("value"));
            Assert::AreEqual(6, *copy_map.find("value"));
            Assert::IsFalse(copy_map.contains("another_value"));
        }

        TEST_METHOD(TrieMap_constructor_move)
        {
            TrieMap<int> map;
            map.try_set("value", 6);
            map.try_set("another_value", 10);

            TrieMap<int> copy_map(std::move(map));
            map.~map();
            Assert::IsTrue(copy_map.contains("value"));
            Assert::AreEqual(6, *copy_map.find("value"));
            Assert::AreEqual(10, *copy_map.find("another_value"));
        }

        TEST_METHOD(TrieMap_operator_assign_copy)
        {
            TrieMap<int> map;
            TrieMap<int> copy_map;

            map.try_set("value", 6);
            map.try_set("another_value", 10);

            copy_map.try_set("extra_value", 64);

            copy_map = map;

            copy_map.try_set("unique_value", 13);

            Assert::IsTrue(copy_map.contains("value"));
            Assert::IsFalse(copy_map.contains("extra_value"));
            Assert::AreEqual(6, *copy_map.find("value"));
            Assert::AreEqual(10, *copy_map.find("another_value"));
            Assert::AreEqual(13, *copy_map.find("unique_value"));
            Assert::IsTrue(map.find("unique_value") == nullptr);
        }

        TEST_METHOD(TrieMap_operator_assign_move)
        {
            TrieMap<int> map;
            TrieMap<int> copy_map;

            map.try_set("value", 6);
            map.try_set("another_value", 10);

            copy_map.try_set("extra_value", 64);

            copy_map = std::move(map);
            map.~map();

            Assert::IsTrue(copy_map.contains("value"));
            Assert::IsFalse(copy_map.contains("extra_value"));
            Assert::IsTrue(*copy_map.find("value") == 6);
            Assert::IsTrue(*copy_map.find("another_value") == 10);
        }

        TEST_METHOD(TrieMap_try_set)
        {
            TrieMap<int> map;
            Assert::IsFalse(map.contains("test"));
            Assert::IsTrue(map.try_set("testing", 200));
            Assert::IsTrue(map.try_set("test", 1234));
            Assert::IsFalse(map.try_set("test", 5432));
            Assert::IsTrue(map.contains("test"));
            Assert::AreEqual(1234, *map.find("test"));
        }

        TEST_METHOD(TrieMap_set)
        {
            TrieMap<int> map;
            Assert::IsFalse(map.contains("test"));
            map.set("testing", 200);
            map.set("test", 1234);
            map.set("test", 5432);
            Assert::IsTrue(map.contains("test"));
            Assert::AreEqual(5432, *map.find("test"));
            Assert::AreEqual(200, *map.find("testing"));
        }

        TEST_METHOD(TrieMap_contains)
        {
            TrieMap<int> map;
            Assert::IsFalse(map.contains("test"));
            Assert::IsFalse(map.contains("testing"));
            Assert::IsFalse(map.contains("test all the things"));
            map.try_set("test", 1);
            map.try_set("testing", 2);
            map.try_set("test all the things", 3);
            Assert::IsTrue(map.contains("test"));
            Assert::IsTrue(map.contains("testing"));
            Assert::IsTrue(map.contains("test all the things"));
        }

        TEST_METHOD(TrieMap_find)
        {
            TrieMap<int> map;
            Assert::IsTrue(map.find("test") == nullptr);
            Assert::IsTrue(map.find("testing") == nullptr);
            Assert::IsTrue(map.find("test all the thigns") == nullptr);

            map.try_set("test", 1);
            map.try_set("testing", 2);
            map.try_set("test all the things", 3);
            Assert::AreEqual(1, *map.find("test"));
            Assert::AreEqual(2, *map.find("testing"));
            Assert::AreEqual(3, *map.find("test all the things"));
            Assert::IsTrue(map.find("testw") == nullptr);
        }

        TEST_METHOD(TrieMap_remove)
        {
            TrieMap<int> map;
            Assert::IsFalse(map.remove("test"));
            Assert::IsFalse(map.remove("testing"));
            Assert::IsFalse(map.remove("test all the things"));
            map.try_set("test", 1);
            map.try_set("testing", 2);
            map.try_set("test all the things", 3);
            Assert::IsTrue(map.remove("test"));
            Assert::IsTrue(map.remove("testing"));
            Assert::IsTrue(map.remove("test all the things"));
        }

        TEST_METHOD(TrieMap_operator_index)
        {
            TrieMap<int> map;
            Assert::IsFalse(map.contains("value"));
            Assert::IsFalse(map.contains("another_value"));

            map["value"] = 6;
            map["another_value"] = 12;

            Assert::IsTrue(map.contains("value"));
            Assert::IsTrue(map.contains("another_value"));

            Assert::AreEqual(6, map["value"]);
            Assert::AreEqual(12, map["another_value"]);

            map["value"] = 90;
            Assert::AreEqual(90, map["value"]);
        }

        TEST_METHOD(TrieMap_getKeys)
        {
            TrieMap<int> map;
            TrieMap<int>::KeyList keys = map.getKeys();
            Assert::AreEqual(0U, keys.size());
            
            map["value"] = 6;
            map["type"] = 20;
            map["another value"] = 36;

            keys = map.getKeys();
            Assert::AreEqual(3U, keys.size());
            Assert::IsTrue(keys.contains("value"));
            Assert::IsTrue(keys.contains("type"));
            Assert::IsTrue(keys.contains("another value"));
        }

        TEST_METHOD(TrieMap_getValues)
        {
            TrieMap<int> map;
            TrieMap<int>::ValueList values = map.getValues();
            Assert::AreEqual(0U, values.size());

            map["value"] = 6;
            map["type"] = 20;
            map["another value"] = 36;

            values = map.getValues();
            Assert::AreEqual(3U, values.size());
            Assert::IsTrue(values.contains(36));
            Assert::IsTrue(values.contains(6));
            Assert::IsTrue(values.contains(20));
        }
    };
}
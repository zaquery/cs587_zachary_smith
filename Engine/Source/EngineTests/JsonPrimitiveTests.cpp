#include "stdafx.h"
#include "CppUnitTest.h"

#include "Engine/Json.h"

#include "JsonTypesToString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Engine;

namespace EngineTests
{
    TEST_CLASS(JsonPrimitiveTests)
    {
    public:
        
        TEST_METHOD(JsonPrimitive_type)
        {
            JsonPrimitive element("test");
            Assert::AreEqual(JsonElement::Type::PRIMITIVE, element.type());
        }

        TEST_METHOD(JsonPrimitive_asBool)
        {
            JsonPrimitive element("true");
            Assert::IsTrue(element.asBool());

            JsonPrimitive element1("false");
            Assert::IsFalse(element1.asBool());
        }

        TEST_METHOD(JsonPrimitive_asInt)
        {
            JsonPrimitive element("123");
            Assert::AreEqual(123, element.asInt());

            JsonPrimitive element1("-123");
            Assert::AreEqual(-123, element1.asInt());
        }

        TEST_METHOD(JsonPrimitive_asLong)
        {
            JsonPrimitive element("1234567890");
            Assert::AreEqual(1234567890L, element.asLong());

            JsonPrimitive element1("-1234567890");
            Assert::AreEqual(-1234567890L, element1.asLong());
        }

        TEST_METHOD(JsonPrimitive_asFloat)
        {
            JsonPrimitive element("123.456");
            Assert::AreEqual(123.456F, element.asFloat());

            JsonPrimitive element1("-1233.4587");
            Assert::AreEqual(-1233.4587F, element1.asFloat());
        }

        TEST_METHOD(JsonPrimitive_asDouble)
        {
            JsonPrimitive element("123.456");
            Assert::AreEqual(123.456, element.asDouble());

            JsonPrimitive element1("-1233.4587");
            Assert::AreEqual(-1233.4587, element1.asDouble());
        }

        TEST_METHOD(JsonPrimitive_asString)
        {
            JsonPrimitive element("\"This is a test string\"");
            Assert::AreEqual(std::string("This is a test string"), element.asString());
        }

        TEST_METHOD(JsonPrimitive_isBool)
        {
            JsonPrimitive element("false");
            Assert::IsTrue(element.isBool());

            JsonPrimitive element1("123");
            Assert::IsFalse(element.isBool());
        }

        TEST_METHOD(JsonPrimitive_isNumber)
        {
            JsonPrimitive element("123");
            Assert::IsTrue(element.isNumber());

            JsonPrimitive element1("false");
            Assert::IsFalse(element1.isNumber());
        }

        TEST_METHOD(JsonPrimitive_isString)
        {
            JsonPrimitive element("\"123\"");
            Assert::IsTrue(element.isString());

            JsonPrimitive element1("false");
            Assert::IsFalse(element1.isString());
        }
    };
}
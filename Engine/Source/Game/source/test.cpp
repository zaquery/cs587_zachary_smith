#include <iostream>

#include <SFML\Graphics.hpp>

#include "Game.h"
#include "Controllers\TestController.h" 

int main()
{
    using namespace Engine;
    using namespace Shooter;

    ActorFactory::instance()
        .addComponentFactory("TestController", createComponent<TestController>);

    Game::instance().init();
    Game::instance().run();
    Game::instance().shutdown();

    return 0;
}
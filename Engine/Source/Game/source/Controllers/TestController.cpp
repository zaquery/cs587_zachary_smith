#include "TestController.h"

namespace Shooter
{
    bool TestController::init(Engine::JsonElement* data)
    {
        _speed = data->asObject()->find("speed")->asPrimitive()->asFloat();
        _degreesPerSecond = data->asObject()->find("rps")->asPrimitive()->asFloat() * 360;
        return true;
    }

    void TestController::tick(float deltaTime)
    {
        using namespace Engine;

        float rotationDelta = _degreesPerSecond * deltaTime;
        owner()->transform()->rotate(rotationDelta);


        //if (InputManager::isKeyPressed(Keyboard::W))
        //{
        //    owner()->transform()->move(0, -_speed * deltaTime);
        //}
        //if (InputManager::isKeyPressed(Keyboard::S))
        //{
        //    owner()->transform()->move(0, _speed * deltaTime);
        //}
        //if (InputManager::isKeyPressed(Keyboard::A))
        //{
        //    owner()->transform()->move(-_speed * deltaTime, 0);
        //}
        //if (InputManager::isKeyPressed(Keyboard::D))
        //{
        //    owner()->transform()->move(_speed * deltaTime, 0);
        //}

        //owner()->transform()->rotate(rotationDelta);

        if (InputManager::isKeyPressed(Keyboard::W))
        {
            owner()->physics()->velocity.y = -_speed;
        }
        if (InputManager::isKeyPressed(Keyboard::S))
        {
            owner()->physics()->velocity.y = _speed;
        }
        if (InputManager::isKeyPressed(Keyboard::A))
        {
            owner()->physics()->velocity.x = -_speed;
        }
        if (InputManager::isKeyPressed(Keyboard::D))
        {
            owner()->physics()->velocity.x = _speed;
        }

        owner()->physics()->velocity *= 1 - (.9 * deltaTime);
    }

    Engine::Component* TestController::clone(void)
    {
        return new TestController(*this);
    }
}
#ifndef TEST_CONTROLLER_H
#define TEST_CONTROLLER_H

#include "Json.h"
#include "Input.h"
#include "Actor\Actor.h"
#include "Actor\ActorController.h"

namespace Shooter
{
	class TestController
		: public Engine::ActorController
	{
		virtual bool init(Engine::JsonElement* data);
		virtual Component* clone(void);
		virtual void tick(float deltaTime);

	private:
        float _speed;
		float _degreesPerSecond;
	};
}

#endif // TEST_CONTROLLER_H
#ifndef INPUT_H
#define INPUT_H

#include "Input\KeyData.h"
#include "Input\MouseData.h"
#include "Input\InputEvent.h"
#include "Input\InputManager.h"

#endif // INPUT_H
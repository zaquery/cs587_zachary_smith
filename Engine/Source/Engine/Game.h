#ifndef GAME_H
#define GAME_H

#include <string>

#include "Input.h"
#include "Scene\Scene.h"
#include "Manager\SceneManager.h"
#include "System\Renderer.h"
#include "Collections\DynamicArray.h"

namespace Engine
{
    class Game
        : public Singleton<Game>
    {
    public:
        bool init(void);
        bool shutdown(void);
        void run(void);

        sf::RenderWindow*   window(void);
        std::string&        title(void);
        Renderer&           renderer(void);

    private:
        std::string         _title;
        sf::RenderWindow    _window;
        Renderer            _renderer;

        bool                _isRunning;
    };
}

#endif // GAME_H
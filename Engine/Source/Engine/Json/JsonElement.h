#ifndef JSON_ELEMENT_HPP
#define JSON_ELEMENT_HPP

#include <string>

namespace Engine
{
    class JsonPrimitive;
    class JsonArray;
    class JsonObject;

    /*
    Represents a single element of parsed Json.
    */
    class JsonElement
    {
    public:
        enum class Type : char
        {
            PRIMITIVE, ARRAY, OBJECT, NOTHING
        };
        JsonElement(void);

        // Virtual

        virtual Type    type(void) const;       // Type JsonElement represents
        virtual         ~JsonElement(void);

        // convenience Casters
        JsonPrimitive*  asPrimitive(void) const;    // convenience method to cast to JsonPrimitive
        JsonArray*      asArray(void) const;        // convenience method to cast to JsonArray
        JsonObject*     asObject(void) const;       // convenience method to cast to JsonObject
        bool            isPrimitive(void) const;
        bool            isArray(void) const;
        bool            isObject(void) const;
    };
} // namespace Engine

#endif // JSON_ELEMENT_H

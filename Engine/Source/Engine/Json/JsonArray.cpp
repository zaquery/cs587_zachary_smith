#include "JsonArray.h"

namespace Engine
{
    JsonArray::JsonArray(void) = default;
    JsonArray::~JsonArray(void)
    {
        for (int i = 0; i < _elements.size(); ++i)
        {
            delete _elements[i];
        }
    }

    JsonElement::Type JsonArray::type(void) const
    {
        return Type::ARRAY;
    }

    unsigned JsonArray::count(void) const
    {
        return _elements.size();
    }

    JsonElement* JsonArray::get(unsigned index)
    {
        return _elements[index];
    }

    void JsonArray::append(JsonElement* element)
    {
        _elements.pushBack(element);
    }
} // namespace Engine
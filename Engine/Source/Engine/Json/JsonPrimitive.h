#ifndef JSON_PRIMITIVE_HPP
#define JSON_PRIMITIVE_HPP

#include <string>
#include <cassert>

#include "JsonElement.h"

namespace Engine
{
    class JsonPrimitive
        : public JsonElement
    {
    public:
        // Constructors
        JsonPrimitive(const std::string& value);

        // Accessors
        virtual Type        type(void)      const;
        bool        asBool(void)    const;
        int         asInt(void)     const;
        long        asLong(void)    const;
        float       asFloat(void)   const;
        double      asDouble(void)  const;
        std::string asString(void)  const;

        // Value Checks
        bool        isNumber(void)  const;
        bool        isBool(void)    const;
        bool        isString(void)  const;
    private:
        std::string _data;
    };
}

#endif // JSON_PRIMITIVE_HPP
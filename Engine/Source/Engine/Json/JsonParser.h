#ifndef JSON_PARSER_HPP
#define JSON_PARSER_HPP

#include <sstream>
#include <fstream>

#include "JsonElement.h"
#include "JsonPrimitive.h"
#include "JsonArray.h"
#include "JsonObject.h"
#include "JsonTokenizer.h"

namespace Engine
{
    class JsonParser
    {
    public:
        static JsonElement*     parse(JsonTokenizer& input);                   // Return next element in tokenizer
        static JsonElement*     parse(const std::string& input);               // Return first element in input string
        static JsonElement*     parseFromFile(const std::string& fileName);    // Return first element in input file
    private:
        static JsonElement*     _createElement(JsonTokenizer& input);
        static JsonPrimitive*   _createPrimitive(JsonTokenizer& input);
        static JsonArray*       _createArray(JsonTokenizer& input);
        static JsonObject*      _createObject(JsonTokenizer& input);
    };
} // namespace Engine
#endif // JSON_PARSER_HPP
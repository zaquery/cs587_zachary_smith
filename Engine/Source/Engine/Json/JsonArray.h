#ifndef JSON_ARRAY_H
#define JSON_ARRAY_H

#include <string>

#include "JsonElement.h"
#include "../Collections/DynamicArray.h"

namespace Engine
{
    /*
    Represents a JsonArray. JsonElements passed in through append are deleted once the array goes
    out of scope.
    */
    class JsonArray
        : public JsonElement
    {
    public:
        //Constructor
        JsonArray();
        ~JsonArray();
        
        // Accessors
        Type            type(void) const;
        unsigned        count(void) const;
        JsonElement*    get(unsigned index);
        void            append(JsonElement* element);
    private:
        DynamicArray<JsonElement*> _elements;
    };
} // Namespace Engine

#endif // JSON_ARRAY_H
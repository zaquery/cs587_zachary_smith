#include "JsonElement.h"

namespace Engine
{
    JsonElement::JsonElement(void) = default;
    JsonElement::~JsonElement(void) = default;

    JsonElement::Type JsonElement::type(void) const
    {
        return Type::NOTHING;
    }

    JsonPrimitive* JsonElement::asPrimitive(void) const
    {
        return (JsonPrimitive*) this;
    }

    JsonArray* JsonElement::asArray(void) const
    {
        return (JsonArray*) this;
    }

    JsonObject* JsonElement::asObject(void) const
    {
        return (JsonObject*) this;
    }

    bool JsonElement::isPrimitive(void) const
    {
        return type() == Type::PRIMITIVE;
    }

    bool JsonElement::isArray(void) const
    {
        return type() == Type::ARRAY;
    }

    bool JsonElement::isObject(void) const
    {
        return type() == Type::OBJECT;
    }
} // namespace Engine
#ifndef JSON_TOKENIZER_HPP
#define JSON_TOKENIZER_HPP

#include <istream>
#include <string>
#include <cassert>

namespace Engine
{
    /*
    Parses an input stream into Json tokens.
    */
    class JsonTokenizer
    {
    public:
        enum class TokenType : char
        {
            ARRAY_BEGIN, ARRAY_END, OBJECT_BEGIN, OBJECT_END, DOCUMENT_END, PROPERTY, VALUE, NULL_VALUE
        };
        // Constructor
        JsonTokenizer(void);
        ~JsonTokenizer(void);

        void            startReading(std::istream* input, bool manageStream = false);
        void            close(void);

        // Token Operations
        TokenType       peek(void);
        bool            hasNext(void);
        std::string     getNextName(void);
        std::string     getNextValue(void);

        void            startArray(void);
        void            endArray(void);
        void            startObject(void);
        void            endObject(void);
    private:
        std::string     _currentToken;
        TokenType       _currentTokenType;
        bool            _currentTokenIsValid;
      
        std::istream*   _input;
        bool            _manageStream;

        void            _resetValues(void);
        bool            _getNextToken(void);            // Return false when end of stream reached
        bool            _isWhitespace(char ch);
        bool            _isStoppingCharacter(char ch);
        void            _consumeWhitespace(void);       // Removes whitespace from beginning of _input
       
        static const std::string _STOPPING_CHARACTERS;
        static const std::string _WHITESPACE_CHARACTERS;
    };
} // namespace Engine

#endif // JSON_TOKENIZER_HPP
#ifndef JSON_OBJECT_HPP
#define JSON_OBJECT_HPP

#include <string>
#include <cassert>

#include "JsonElement.h"
#include "../Collections/TrieMap.h"

namespace Engine
{
    class JsonObject
        : public JsonElement
    {
    public:
        virtual         ~JsonObject(void);
        Type            type(void) const;
        JsonElement*    find(const std::string& propertyName);
        bool            has(const std::string& propertyName) const;
        void            add(const std::string& propertyName,
                            JsonElement* element);
    private:
        TrieMap<JsonElement*> _properties;
    };
} // namespace Engine

#endif // JSON_OBJECT_HPP
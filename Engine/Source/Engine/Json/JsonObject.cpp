#include "JsonObject.h"

namespace Engine
{
    JsonObject::~JsonObject(void)
    {
        auto values = _properties.getValues();
        for (int i = 0; i < values.size(); ++i)
        {
            delete values[i];
        }
    }

    JsonElement::Type JsonObject::type(void) const
    {
        return Type::OBJECT;
    }

    JsonElement* JsonObject::find(const std::string& propertyName)
    {
        JsonElement** rtn = _properties.find(propertyName);
        if (rtn == nullptr)
        {
            return nullptr;
        }
        else
        {
            return *rtn;
        }
    }

    bool JsonObject::has(const std::string& propertyName) const
    {
        return _properties.contains(propertyName);
    }

    void JsonObject::add(const std::string& propertyName, JsonElement* element)
    {
        assert(_properties.contains(propertyName) == false);
        _properties.set(propertyName, element);
    }
} // namespace Engine
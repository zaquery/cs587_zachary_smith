#include "JsonPrimitive.h"

namespace Engine
{
    JsonPrimitive::JsonPrimitive(const std::string& value)
        : _data(value)
    { /* EMPTY */ }

    //////////////////////////////////////////////////////////
    // Token Operations
    //////////////////////////////////////////////////////////

    JsonElement::Type JsonPrimitive::type(void) const
    {
        return Type::PRIMITIVE;
    }

    bool JsonPrimitive::asBool(void) const
    {
        assert(_data == "true" || _data == "false");
        return _data == "true";
    }

    int JsonPrimitive::asInt(void) const
    {
        return std::stoi(_data);
    }

    long JsonPrimitive::asLong(void) const
    {
        return std::stol(_data);
    }

    float JsonPrimitive::asFloat(void) const
    {
        return std::stof(_data);
    }

    double JsonPrimitive::asDouble(void) const
    {
        return std::stod(_data);
    }

    std::string JsonPrimitive::asString(void) const
    {
        // strip front and back quotations from value
        return _data.substr(1, _data.size() - 2);
    }

    //////////////////////////////////////////////////////////
    // Value Checks
    //////////////////////////////////////////////////////////

    bool JsonPrimitive::isNumber(void) const
    {
        return false;
    }

    bool JsonPrimitive::isBool(void) const
    {
        return false;
    }

    bool JsonPrimitive::isString(void) const
    {
        return false;
    }
} // namespace Engine
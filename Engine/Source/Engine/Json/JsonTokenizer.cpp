#include "JsonTokenizer.h"
#include "../Util.h"

namespace Engine
{
    JsonTokenizer::JsonTokenizer(void)
    {
        _resetValues();
    }

    JsonTokenizer::~JsonTokenizer(void)
    {
        close();
    }

    void JsonTokenizer::startReading(std::istream* input, bool manageStream)
    {
        _resetValues();
        _input = input;
        _manageStream = manageStream;
    }

    void JsonTokenizer::close(void)
    {
        if (_manageStream)
        {
            delete _input;
        }
    }

    JsonTokenizer::TokenType JsonTokenizer::peek(void)
    {
        if (_currentTokenIsValid == false)
        {
            if (_getNextToken() == false)
            {
                _currentTokenType = TokenType::DOCUMENT_END;
            }
            else
            {
                switch (_currentToken.back())
                {
                case '{':
                    _currentTokenType = TokenType::OBJECT_BEGIN;
                    break;
                case '}':
                    _currentTokenType = TokenType::OBJECT_END;
                    break;
                case '[':
                    _currentTokenType = TokenType::ARRAY_BEGIN;
                    break;
                case ']':
                    _currentTokenType = TokenType::ARRAY_END;
                    break;
                case ':':
                    _currentTokenType = TokenType::PROPERTY;
                    break;
                default:
                    _currentTokenType = TokenType::VALUE;
                }
            }
            _currentTokenIsValid = true;
        }

        return _currentTokenType;
    }

    bool JsonTokenizer::hasNext(void)
    {
        TokenType type = peek();

        return
            type != TokenType::OBJECT_END     &&
            type != TokenType::ARRAY_END      &&
            type != TokenType::DOCUMENT_END;
    }

    std::string JsonTokenizer::getNextName(void)
    {
        if (_currentTokenIsValid == false)
        {
            _getNextToken();
        }
        _currentTokenIsValid = false;
        // Strip quotations (1 from front, 1 from back)
        return _currentToken.substr(1, _currentToken.size() - 3);
    }

    std::string JsonTokenizer::getNextValue(void)
    {
        if (_currentTokenIsValid == false) 
        {
            _getNextToken();
        }
        _currentTokenIsValid = false;

        return _currentToken;
    }

    void JsonTokenizer::startArray(void)
    {
        TokenType type = peek();
        assert(type == TokenType::ARRAY_BEGIN);
        if (peek() == TokenType::ARRAY_BEGIN)
        {
            _currentTokenIsValid = false;
        }
    }

    void JsonTokenizer::endArray(void)
    {
        TokenType type = peek();
        assert(type == TokenType::ARRAY_END);
        _currentTokenIsValid = false;
    }

    void JsonTokenizer::startObject(void)
    {
        TokenType type = peek();
        assert(type == TokenType::OBJECT_BEGIN);
        _currentTokenIsValid = false;
    }

    void JsonTokenizer::endObject(void)
    {
        TokenType type = peek();
        assert(type == TokenType::OBJECT_END);
        _currentTokenIsValid = false;
    }

    //////////////////////////////////////////////////////////
    // Private
    //////////////////////////////////////////////////////////

    const std::string JsonTokenizer::_STOPPING_CHARACTERS   = std::string("[]{}");
    const std::string JsonTokenizer::_WHITESPACE_CHARACTERS = std::string(" \r\n\t,:");

    void JsonTokenizer::_resetValues(void)
    {
        _currentTokenIsValid = false;
        _manageStream = false;
    }

    bool JsonTokenizer::_getNextToken(void)
    {
        _currentToken.clear();
        _consumeWhitespace();

        if (_input->eof())
        {
            return false;
        }

        bool inString = false;
        char currentChar = _input->peek();
        if (_isStoppingCharacter(currentChar))
        {
            _currentToken.push_back(_input->get());
        }
        else
        {
            while (_input->eof() == false)
            {
                if (_isWhitespace(currentChar) && inString == false)
                {
                    break;
                }
                else if (_isStoppingCharacter(currentChar))
                {
                    break;
                } 
                else if (currentChar == ':')
                {
                    _currentToken.push_back(_input->get());
                    break;
                }

                _currentToken.push_back(_input->get());

                if (currentChar == '\"')
                {
                    if (inString && _currentToken.back() != '\\')
                    {
                        inString = false;
                    }
                    else
                    {
                        inString = true;
                    }
                }

                currentChar = _input->peek();
            }
        }
        return true;
    }

    bool JsonTokenizer::_isWhitespace(char ch)
    {
        switch (ch)
        {
        case ' ':
        case '\r':
        case '\n':
        case '\t':
        case ',':
            return true;
        default:
            return false;
        }
    }

    bool JsonTokenizer::_isStoppingCharacter(char ch)
    {
        switch (ch)
        {
        case '{':
        case '}':
        case '[':
        case ']':
            return true;
        default:
            return false;
        }
    }

    void JsonTokenizer::_consumeWhitespace(void)
    {
        while (_isWhitespace(_input->peek()))
        {
            _input->get();
        }
    }
} //namespace Engine
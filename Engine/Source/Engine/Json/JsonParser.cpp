#include "JsonParser.h"

namespace Engine
{

    //////////////////////////////////////////////////////////
    // Public
    //////////////////////////////////////////////////////////

    JsonElement* JsonParser::parse(JsonTokenizer& input)
    {
        if (input.peek() == JsonTokenizer::TokenType::DOCUMENT_END)
        {
            return nullptr;
        }
        else
        {
            return _createElement(input);
        }
    }

    JsonElement* JsonParser::parse(const std::string& input)
    {
        JsonTokenizer tokenizer;
        std::stringstream stream(input);
        tokenizer.startReading(&stream);
        return parse(tokenizer);
    }

    JsonElement* JsonParser::parseFromFile(const std::string& fileName)
    {
        JsonTokenizer tokenizer;
        std::ifstream stream(fileName);
        if (stream.is_open() == false)
        {
            return nullptr;
        }
        else
        {
            tokenizer.startReading(&stream);
            return parse(tokenizer);
        }
    }

    //////////////////////////////////////////////////////////
    // Private
    //////////////////////////////////////////////////////////

    JsonElement* JsonParser::_createElement(JsonTokenizer& input)
    {
        JsonTokenizer::TokenType type = input.peek();
        switch (type)
        {
            // TODO: figure out the goddamn dynamic cast shit
        case JsonTokenizer::TokenType::ARRAY_BEGIN:
            return _createArray(input);
        case JsonTokenizer::TokenType::OBJECT_BEGIN:
            return _createObject(input);
        case JsonTokenizer::TokenType::VALUE:
            return _createPrimitive(input);
        }
    }

    JsonPrimitive* JsonParser::_createPrimitive(JsonTokenizer& input)
    {
        return new JsonPrimitive(input.getNextValue());
    }

    JsonArray* JsonParser::_createArray(JsonTokenizer& input)
    {
        JsonArray* jarray = new JsonArray();
        input.startArray();
        while (input.hasNext())
        {
            jarray->append(_createElement(input));
        }
        input.endArray();
        return jarray;
    }

    JsonObject* JsonParser::_createObject(JsonTokenizer& input)
    {
        JsonObject* jobject = new JsonObject();
        input.startObject();
        while (input.hasNext())
        {
            std::string propertyName = input.getNextName();
            jobject->add(
                propertyName,
                _createElement(input)
                );
        }
        input.endObject();
        return jobject;
    }
} // namespace Engine
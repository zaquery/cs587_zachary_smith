#ifndef ACTOR_MANAGER_H
#define ACTOR_MANAGER_H

#include "Actor\Actor.h"
#include "Actor\ActorFactory.h"
#include "Json.h"

namespace Engine
{
    class ActorManager
    {
    public:
        ActorManager(void)		= default;
        ~ActorManager(void);
		
        bool					init(JsonElement* data);
        ActorPtr				instantiate(ActorPtr original, Transform* transform);
		ActorPtr				add(ActorPtr actor);
        void					remove(ActorPtr actor);
        ActorPtr				getPrototype(const std::string& prototypeName);
		DynamicArray<ActorPtr>&	actors(void);

        Physics* physics;

    private:
        DynamicArray<ActorPtr>  _actors;
        TrieMap<ActorPtr>		_prototypes;
    };
}

#endif // ACTOR_MANAGER_H
#include "ActorManager.h"
#include "Game.h"

namespace Engine
{
    bool ActorManager::init(JsonElement* data)
    {
        JsonArray* array = data->asArray();
        for (uint index = 0; index < array->count(); index++)
        {
            JsonObject* actorData = array->get(index)->asObject();
            JsonPrimitive* ActorPtrerence = actorData->find("reference")->asPrimitive();
            Actor* actor;
            if (ActorPtrerence != nullptr)
            {
                actor = ActorFactory::instance().createActor(ActorPtrerence->asString(), true);
            }
            else
            {
                actor = ActorFactory::instance().createActor(actorData->find("data")->asObject(), true);
            }
            if (actor == nullptr)
            {
                return false;
            }
            std::string actorName = actorData->find("name")->asPrimitive()->asString();
            _prototypes.set(actorName, actor);
        }

        return true;
    }

    ActorPtr ActorManager::instantiate(ActorPtr original, Transform* transform)
    {
        ActorPtr clone = ActorFactory::instance().clone(original, transform);
        add(clone);
        return clone;
    }

    ActorPtr ActorManager::add(ActorPtr actor)
    {
        _actors.pushBack(actor);
        auto actorPhysics = actor->physics();
        if (actorPhysics != nullptr)
        {
            physics->registerPhysics(actorPhysics);
        }
        return actor;
    }

    void ActorManager::remove(ActorPtr actor)
    {
        for (int index = 0; index < _actors.size(); index++)
        {
            ActorPtr tempActor = _actors[index];

            if (tempActor->id() == actor->id())
            {
                _actors[index] = _actors.back();	// replace with last actor in list
                if (_actors.size() > 0)
                {
                    _actors.popBack();				// remove duplicate
                }
                auto actorPhysics = tempActor->physics();
                if (actorPhysics == nullptr)
                {
                    physics->unregisterPhysics(actorPhysics);
                }

                delete tempActor;
                break;
            }
        }
    }

    Actor* ActorManager::getPrototype(const std::string& prototypeName)
    {
        return *_prototypes.find(prototypeName);
    }

    DynamicArray<ActorPtr>& ActorManager::actors(void)
    {
        return _actors;
    }

    ActorManager::~ActorManager(void)
    {
        auto prototypes = _prototypes.getValues();
        for (int index = 0; index < prototypes.size(); index++)
        {
            delete prototypes[index];
        }

        for (int index = 0; index < _actors.size(); index++)
        {
            delete _actors[index];
        }
    }
}
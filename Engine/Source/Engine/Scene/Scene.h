#ifndef SCENE_H
#define SCENE_H

#include <SFML\Graphics.hpp>

#include "Json.h"
#include "Actor\IDrawable.h"
#include "Actor\Actor.h"
#include "Scene\ActorManager.h"
#include "ITickable.h"
#include "Collections\DynamicArray.h"
#include "System\Physics.h"

namespace Engine
{
    class Scene
    {
    public:
        // Constructors
        Scene(void);

        // Setup/Teardown
        bool init(JsonElement* data);
        bool onFinish(void);
        //virtual bool onStart(void);

        // Runtime Operations
        void advance(float dt);
        void addActor(Actor* actor);            // Add actor to scene
        
		ActorManager& actorManager(void);
        DynamicArray<ActorPtr>& actors(void);

    private:
		void _initActors(JsonArray* data);

		ActorManager    _actorManager;
        Physics         _physics;
    };
} 

#endif // SCENE_H
#include "Scene.h"
#include "Game.h"

namespace Engine
{
	//////////////////////////////////////////////////////////
	// Constructors
	//////////////////////////////////////////////////////////

	Scene::Scene(void) = default;

	//////////////////////////////////////////////////////////
	// Setup/Teardown
	//////////////////////////////////////////////////////////

	bool Scene::init(JsonElement* data)
	{
        JsonElement* physicsData = data->asObject()->find("physics");
        assert(physicsData != nullptr);
        _physics.init(physicsData);

        _actorManager.physics = &_physics;
		
        JsonElement* prototypeData = data->asObject()->find("prototypes");
		assert(prototypeData != nullptr);
		_actorManager.init(prototypeData);

		JsonElement* actorData = data->asObject()->find("actors");
		assert(actorData != nullptr);
		_initActors(actorData->asArray());

        return true;
	}

    bool Scene::onFinish(void)
    {
        _physics.cleanup();
        return true;
    }

	//////////////////////////////////////////////////////////
	// Runtime Operations
	//////////////////////////////////////////////////////////

	void Scene::advance(float deltaTime)
	{
		auto actorList = _actorManager.actors();
		for (uint index = 0; index < actorList.size(); index++)
		{
			ITickable* controller = actorList[index]->controller();
			if (controller != nullptr)
			{
				controller->tick(deltaTime);
			}
		}
        _physics.tick(deltaTime);
	}

	ActorManager& Scene::actorManager(void)
	{
		return _actorManager;
	}

    DynamicArray<ActorPtr>& Scene::actors(void)
    {
        return _actorManager.actors();
    }

	void Scene::_initActors(JsonArray* data)
	{
		for (uint index = 0; index < data->count(); index++)
		{
			JsonObject* actorData = data->get(index)->asObject();
			JsonPrimitive* actorReference = actorData->find("reference")->asPrimitive();
			
            if (actorReference != nullptr)
			{
                ActorPtr actor = _actorManager.getPrototype(actorReference->asString());
				Transform* transform = new Transform();
				transform->init(actorData->find("transform")->asObject());
				_actorManager.instantiate(actor, transform);
			}
			else
			{
				ActorPtr actor = ActorFactory::instance().createActor(actorData);
				_actorManager.add(actor);
			}
		}
	}
}


#ifndef SINGLETON_H
#define SINGLETON_H

namespace Engine
{
    template <typename BaseType>
    class Singleton
    {
    public:
        Singleton(void) = default;
        static BaseType& instance(void)
        {
            static BaseType instance;
            return instance;
        }

    protected:
    };
}

#endif // SINGLETON_H
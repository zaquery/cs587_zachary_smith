#ifndef ITICKABLE_H
#define ITICKABLE_H

namespace Engine
{
	class ITickable
	{
	public:
		virtual void tick(float deltaTime) = 0;
	};
}

#endif // ITICKABLE_H
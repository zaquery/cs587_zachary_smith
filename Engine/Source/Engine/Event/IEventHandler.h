#ifndef IEVENT_HANDLER_H
#define IEVENT_HANDLER_H

#include "Event.h"

namespace Engine
{
    class IEventHandler
    {
    public:
        virtual void on(Event* event) = 0;
    };
}

#endif // IEVENT_HANDLER_H
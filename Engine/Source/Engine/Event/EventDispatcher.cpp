#include "EventDispatcher.h"

namespace Engine
{
    EventDispatcher::EventDispatcher(unsigned capacity)
        : _capacity(capacity)
        , _events(capacity)
    {
        /* EMPTY */
    }

    EventDispatcher& EventDispatcher::instance(void)
    {
        static EventDispatcher* dispatcher = new EventDispatcher(INSTANCE_CAPACITY);
        return *dispatcher;
    }

    void EventDispatcher::registerHandler(const std::string& type, IEventHandler* handler)
    {
        DynamicArray<IEventHandler*>& handlers = _eventHandlers[type];
        if (handlers.contains(handler))
        {
            return;
        }

        handlers.pushBack(handler);
    }

    void EventDispatcher::unregisterHandler(const std::string& type, IEventHandler* handler)
    {
        DynamicArray<IEventHandler*>& handlers = _eventHandlers[type];
        
        uint index = handlers.find(handler);
        
        if (index != -1)
        {
            handlers[index] = handlers.back();
            handlers.popBack();

            return;
        }

        handlers.pushBack(handler);
    }

    void EventDispatcher::queueEvent(Event* event)
    {
        _events.pushBack(event);
    }

    void EventDispatcher::triggerEvent(Event* event)
    {
        DynamicArray<IEventHandler*>& handlers = _eventHandlers[event->eventType];

        for (unsigned index = 0; index < handlers.size(); index++)
        {
            handlers[index]->on(event);
        }
    }

    void EventDispatcher::triggerAll(void)
    {
        while (_events.size() > 0)
        {
            triggerEvent(_events.front());
            delete _events.front();
            _events.popFront();
        }
    }
}
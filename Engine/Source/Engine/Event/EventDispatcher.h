#ifndef EVENT_DISPATCHER_H
#define EVENT_DISPATCHER_H


#include "IEventHandler.h"
#include "Collections\DynamicArray.h"
#include "Collections\TrieMap.h"
#include "Collections\Queue.h"

namespace Engine
{
    class EventDispatcher
    {
    public:
        EventDispatcher(uint capacity = INSTANCE_CAPACITY);

        static EventDispatcher& instance();

        uint capacity(void);

        void registerHandler(const std::string& type, IEventHandler* handler);
        void unregisterHandler(const std::string& type, IEventHandler* handler);

        void triggerEvent(Event* event);

        void queueEvent(Event* event);
        void triggerAll(void);

    protected:
        static const uint INSTANCE_CAPACITY = 500;
    
    private:
        const unsigned int                      _capacity;
        TrieMap<DynamicArray<IEventHandler*>>   _eventHandlers;
        Queue<Event*>                           _events;
    };
}

#endif // EVENT_DISPATCHER_H
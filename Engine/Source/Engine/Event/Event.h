#ifndef EVENT_H
#define EVENT_H

#include <string>

namespace Engine
{
    struct Event
    {
        std::string eventType;

        Event(const std::string& eventType)
            : eventType(eventType)
        {

        }
    };
}

#endif // EVENT_H
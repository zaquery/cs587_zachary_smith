#ifndef UTIL_H
#define UTIL_H

#include <string>

#include "SFML\Window.hpp"
#include "SFML\Graphics.hpp"

namespace Engine
{
    namespace Util
    {
        template <typename Type>
        void swap(Type& a, Type& b)
        {
            Type c = std::move(a);
            a = std::move(b);
            b = std::move(c);
        }

        inline bool contains(const std::string& str, char ch)
        {
            return str.find(ch) != std::string::npos;
        }

        template <typename ValueType>
        ValueType min(ValueType a, ValueType b)
        {
            return (a < b) ? a : b;
        }

        template <typename ValueType>
        ValueType max(ValueType a, ValueType b)
        {
            return (b < a) ? a : b;
        }

        const static float PI           = 3.14159265;
        const static float DEG_TO_RAD   = PI / 180.0f;

    }
}

#endif // UTIL_H

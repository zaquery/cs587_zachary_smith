#ifndef DYNAMIC_ARRAY_H
#define DYNAMIC_ARRAY_H

#include <type_traits>
#include <cassert>

#include "Engine.h"
#include "../Util.h"

namespace Engine
{
    template <typename ValueType>
    class DynamicArray
    {
    public:
        // Constructors
        DynamicArray(void);
        DynamicArray(uint initialCapacity);                         // Create array with capacity equal to initialCapacity
        /*DynamicArray(ValueType* arr,
                        uint size,
                        bool copy_arr = true);    */
        DynamicArray(const DynamicArray& copy);
        DynamicArray(DynamicArray&& move);
        ~DynamicArray(void);

        // Operators
        DynamicArray&       operator=(const DynamicArray& copy);
        DynamicArray&       operator=(DynamicArray&& move);
        ValueType&          operator[](uint index);                 // Return reference to value at index 
        const ValueType&    operator[](uint index) const;           // Return const reference to value at index

        // List Operations
        ValueType           removeAt(uint index); // TODO:          // Remove item at index
        void                fill(const ValueType& fillValue);       // Set every item to fillValue
        uint                find(const ValueType& value) const;     // return index of value, size() if not found
        bool                contains(const ValueType& value) const;

        // Stack/Queue Operations
        ValueType&          back(void);                             // Return reference to last item
        const ValueType&    back(void) const;                       // Return const reference to last item
        void                popBack(void);                          // Remove last item
        void                pushBack(const ValueType&);             // Add item to end

        ValueType&          front(void);                            // Return reference to the first item
        const ValueType&    front(void) const;                      // Return const reference to the first item
        void                popFront(void);
        void                pushFront(void);

        // Size Management
        bool                isEmpty(void) const;                    // Return if empty
        uint                size(void) const;                       // Return current size
        uint                capacity(void) const;                   // Return current capacity
        void                resize(uint newize);                    // Set size to newSize
        void                reserve(uint newCapacity);              // Set capacity to newCapacity

        ValueType*          getData(void);                          // Return raw pointer to the backing array

    private:
        uint                _size;                                  // Current size
        uint                _capacity;                              // Current capacity
        ValueType*          _array;                                 // Backing array

        static const uint _DEFAULT_CAPACITY = 10;
    };

    //////////////////////////////////////////////////////////
    // Constructors
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    DynamicArray<ValueType>::DynamicArray(void)
        : _size(0)
        , _capacity(_DEFAULT_CAPACITY)
    {
        _array = new ValueType[_DEFAULT_CAPACITY];
    }

    template <typename ValueType>
    DynamicArray<ValueType>::DynamicArray(uint initialCapacity)
        : _size(0)
        , _capacity(initialCapacity)
    {
        assert(initialCapacity > 0); // No reason to make a list without ability to hold items
        _array = new ValueType[initialCapacity];
    }

    template <typename ValueType>
    DynamicArray<ValueType>::DynamicArray(const DynamicArray& copy)
        : _size(copy._size)
        , _capacity(copy._capacity)
    {
        _array = new ValueType[copy._capacity];

        // Use memcpy on scalar types and data-structs
        if (std::is_trivially_copyable<ValueType>::value)
        {
            std::memcpy(_array, copy._array, copy._size * sizeof(ValueType));
        }
        else
        {
            for (uint i = 0; i < copy._size; ++i)
            {
                _array[i] = copy._array[i];
            }
        }
    }

    template <typename ValueType>
    DynamicArray<ValueType>::DynamicArray(DynamicArray&& move)
        : _size(move._size)
        , _capacity(move._capacity)
        , _array(move._array)
    {
        move._size = 0;
        move._capacity = 0;
        move._array = nullptr;
    }

    template <typename ValueType>
    DynamicArray<ValueType>::~DynamicArray(void)
    {
        delete[] _array;
    }

    //////////////////////////////////////////////////////////
    // Operators
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    DynamicArray<ValueType>& DynamicArray<ValueType>::operator=(const DynamicArray& copy)
    {
        _size = copy._size;
        _capacity = copy._capacity;
        delete[] _array;

        _array = new ValueType[copy._capacity];

        // TODO: check for plain data - use memcopy
        for (uint i = 0; i < copy._size; ++i)
        {
            _array[i] = copy._array[i];
        }
        return *this;
    }

    template <typename ValueType>
    DynamicArray<ValueType>& DynamicArray<ValueType>::operator=(DynamicArray&& move)
    {
        _size = move._size;
        _capacity = move._capacity;

        // Replace internal array
        delete[] _array;
        _array = move._array;
        move._array = nullptr;

        return *this;
    }

    template <typename ValueType>
    ValueType& DynamicArray<ValueType>::operator[](uint index)
    {
        assert(index >= 0 && index < _size);
        return _array[index];
    }

    template <typename ValueType>
    const ValueType& DynamicArray<ValueType>::operator[](uint index) const
    {
        assert(index >= 0 && index < _size);
        return _array[index];
    }

    //////////////////////////////////////////////////////////
    // List Operations
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    ValueType DynamicArray<ValueType>::removeAt(uint index)
    {
        assert(_size > 0);

        ValueType rtn = _array[index];

        // move element that will be removed to the back
        for (uint i = index; i + 1 < _size; i++)
        {
            _array[i] = _array[i+1];
        }
        _size--;

        return rtn;
    }

    template <typename ValueType>
    void DynamicArray<ValueType>::fill(const ValueType& fillValue)
    {
        for (uint i = 0; i < _size; ++i)
        {
            _array[i] = fillValue;
        }
    }

    template <typename ValueType>
    uint DynamicArray<ValueType>::find(const ValueType& value) const
    {
        for (uint i = 0; i < _size; ++i)
        {
            if (value == _array[i])
            {
                return i;
            }
        }
        return _size;
    }

    template <typename ValueType>
    bool DynamicArray<ValueType>::contains(const ValueType& value) const
    {
        return find(value) != _size;
    }

    //////////////////////////////////////////////////////////
    // Stack/Queue Operations
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    ValueType& DynamicArray<ValueType>::back(void)
    {
        assert(_size > 0);
        return _array[_size - 1];
    }

    template <typename ValueType>
    const ValueType& DynamicArray<ValueType>::back(void) const
    {
        assert(_size > 0);
        return _array[_size - 1];
    }

    template <typename ValueType>
    void DynamicArray<ValueType>::popBack()
    {
        // Simply decrement _size
        assert(_size > 0);
        --_size;
    }

    template <typename ValueType>
    void DynamicArray<ValueType>::pushBack(const ValueType& value)
    {
        if (_size == _capacity)
        {
            // TODO: generate better heuristic
            // for now just double _capacity
            // TODO: add 1 to avoid allocating 0?
            reserve(_capacity * 2);
        }
        _array[_size++] = value;
    }

    //////////////////////////////////////////////////////////
    // Size Management
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    bool DynamicArray<ValueType>::isEmpty(void) const
    {
        return _size == 0;
    }

    template <typename ValueType>
    uint DynamicArray<ValueType>::size(void) const
    {
        return _size;
    }

    template <typename ValueType>
    uint DynamicArray<ValueType>::capacity(void) const
    {
        return _capacity;
    }

    template <typename ValueType>
    void DynamicArray<ValueType>::resize(uint newSize)
    {
        if (newSize > _capacity)
        {
            reserve(newSize);
        }
        _size = newSize;
    }

    template <typename ValueType>
    void DynamicArray<ValueType>::reserve(uint newCapacity)
    {
        assert(newCapacity > _capacity);
        ValueType* temp_array = new ValueType[newCapacity];
        _capacity = newCapacity;

        for (uint i = 0; i < _size; ++i)
        {
            temp_array[i] = std::move(_array[i]);
        }

        delete[] _array;
        _array = temp_array;
    }

    template <typename ValueType>
    ValueType* DynamicArray<ValueType>::getData(void)
    {
        return _array;
    }
} // namespace Engine

#endif // DYNAMIC_ARRAY_H
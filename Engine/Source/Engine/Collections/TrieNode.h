#ifndef TRIE_NODE_HPP
#define TRIE_NODE_HPP

namespace Engine
{
template <typename ValueType>
class TrieNode
{
    typedef unsigned char CharType;
public:
    // Constructors
    TrieNode(void);
    TrieNode(const TrieNode& copy);
    TrieNode(TrieNode&& move);
    ~TrieNode(void);

    // Operators
    TrieNode&   operator=(const TrieNode& copy);
    TrieNode&   operator=(TrieNode&& move);

    // Node Operations
    TrieNode*   get_node(CharType ch) const;
    TrieNode*   add_node(CharType ch);

    // Member Variables
    bool        is_value;
    ValueType   value;

    static const unsigned   VALID_CHAR_COUNT = 256;
    static const char       STARTING_CHAR = 0;
private:
    void        _delete_children(void);
   
    TrieNode*   _children[VALID_CHAR_COUNT];
};


//////////////////////////////////////////////////////////
// Constructors
//////////////////////////////////////////////////////////

template <typename ValueType>
TrieNode<ValueType>::TrieNode(void)
    : is_value(false)
{
    for (unsigned i = 0; i < VALID_CHAR_COUNT; ++i)
    {
        _children[i] = nullptr;
    }
}

template <typename ValueType>
TrieNode<ValueType>::TrieNode(const TrieNode<ValueType>& copy)
    : is_value(copy.is_value)
    , value(copy.value)
{
    for (unsigned i = 0; i < VALID_CHAR_COUNT; ++i)
    {
        _children[i] = (copy._children[i] != nullptr)
            ? new TrieNode(*copy._children[i])
            : nullptr;
    }
}

template <typename ValueType>
TrieNode<ValueType>::TrieNode(TrieNode<ValueType>&& move)
    : is_value(move.is_value)
    , value(std::move(move.value))
{
    std::memcpy(_children, move._children, sizeof _children);
    for (unsigned i = 0; i < VALID_CHAR_COUNT; ++i)
    {
        move._children[i] = nullptr;
    }
}

template <typename ValueType>
TrieNode<ValueType>::~TrieNode(void)
{
    _delete_children();
}


//////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////

template <typename ValueType>
TrieNode<ValueType>& TrieNode<ValueType>::operator=(const TrieNode<ValueType>& copy)
{
    is_value = copy.is_value;
    value = copy.value;

    _delete_children();
    for (unsigned i = 0; i < VALID_CHAR_COUNT; ++i)
    {
        _children[i] = copy._children[i];
    }
    return *this;
}

template <typename ValueType>
TrieNode<ValueType>& TrieNode<ValueType>::operator=(TrieNode<ValueType>&& move)
{
    is_value = copy.is_value;
    value = copy.value;

    _delete_children();
    for (unsigned i = 0; i < VALID_CHAR_COUNT; ++i)
    {
        _children[i] = std::move(copy._children[i]);
    }
    return *this;
}


//////////////////////////////////////////////////////////
// Node Operations
//////////////////////////////////////////////////////////

template <typename ValueType>
TrieNode<ValueType>* TrieNode<ValueType>::get_node(CharType ch) const
{
    return _children[ch];
}

template <typename ValueType>
TrieNode<ValueType>* TrieNode<ValueType>::add_node(CharType ch)
{
    if (_children[ch] == nullptr)
    {
        _children[ch] = new TrieNode();
    }
    return _children[ch];
}


//////////////////////////////////////////////////////////
// Private
//////////////////////////////////////////////////////////

template <typename ValueType>
void TrieNode<ValueType>::_delete_children(void)
{
    for (unsigned i = 0; i < VALID_CHAR_COUNT; ++i)
    {
        delete _children[i];
        _children[i] = nullptr;
    }
}
} // namespace Engine

#endif // TRIE_NODE_HPP
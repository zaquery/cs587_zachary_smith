#ifndef TRIE_TrieMap_HPP
#define TRIE_TrieMap_HPP

#include <cassert>
#include <string>

#include "TrieNode.h"
#include "DynamicArray.h"

namespace Engine
{
    template <typename U, typename V>
    struct Pair
    {
        U first;
        V second;

        Pair(void) = default;
        Pair(const U& u, const V& v)
            : first(u)
            , second(v)
        {
            /* EMPTY */
        }
        
    };

    template <typename ValueType>
    class TrieMap
    {
    public:
        typedef Pair<std::string, ValueType>    KeyValuePair;
        typedef DynamicArray<std::string>       KeyList;
        typedef DynamicArray<ValueType>         ValueList;

        // Constructors
        TrieMap(void);
        TrieMap(const TrieMap& copy);
        TrieMap(TrieMap&& move);

        // Operators
        TrieMap&            operator=(const TrieMap& copy);
        TrieMap&            operator=(TrieMap&& move);
        ValueType&          operator[](const std::string& key);                         // Access value mapped to key, sets key if not already added

        // TrieMap Operations
        bool                try_set(const std::string& key, const ValueType& value);    // Sets key to value if key has not already been set
        void                set(const std::string& key, const ValueType& value);        // Sets value of key to value, overriding any previous value
        bool                contains(const std::string& key) const;
        ValueType*          find(const std::string& key);
        bool                remove(const std::string& key);

        KeyList             getKeys(void) const;
        ValueList           getValues(void) const;

    private:
        TrieNode<uint>*             _getNode(const std::string& key, bool createNodes) const;  // Returns node for the given key. nullptr if no node exists and create_nodes is not true
        uint                        _add(const std::string& key, const ValueType& value);
        ValueType&                  _remove(uint index);

        TrieNode<uint>*             _root;
        DynamicArray<KeyValuePair>  _keyValues;
        DynamicArray<uint>          _openIndices;
    };


    //////////////////////////////////////////////////////////
    // Constructors
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    TrieMap<ValueType>::TrieMap(void)
        : _root(new TrieNode<uint>())
    {
        /*Empty*/
    }

    template <typename ValueType>
    TrieMap<ValueType>::TrieMap(const TrieMap& copy)
        : _root(new TrieNode<uint>(*copy._root))
        , _keyValues(copy._keyValues)
        , _openIndices(copy._openIndices)
    {
        /*Empty*/
    }

    template <typename ValueType>
    TrieMap<ValueType>::TrieMap(TrieMap<ValueType>&& move)
    {
        _root = std::move(move._root);
        _keyValues = std::move(move._keyValues);
        _openIndices = std::move(move._openIndices);
        move._root = nullptr;
    }


    //////////////////////////////////////////////////////////
    // Operators
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    TrieMap<ValueType>& TrieMap<ValueType>::operator=(const TrieMap<ValueType>& copy)
    {
        *_root = *copy._root;
        _keyValues = copy._keyValues;
        _openIndices = copy._openIndices;
        return *this;
    }

    template <typename ValueType>
    TrieMap<ValueType>& TrieMap<ValueType>::operator=(TrieMap&& move)
    {
        delete _root;
        _root = move._root;
        _keyValues = std::move(move._keyValues);
        _openIndices = std::move(move._openIndices);
        move._root = nullptr;
        return *this;
    }

    template <typename ValueType>
    ValueType& TrieMap<ValueType>::operator[](const std::string& key)
    {
        TrieNode<uint>* node = _getNode(key, true);
        if (node->is_value == false)
        {
            node->value = _add(key, ValueType());
            node->is_value = true;
        }
        return _keyValues[node->value].second;
    }

    //////////////////////////////////////////////////////////
    // TrieMap Operations
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    bool TrieMap<ValueType>::try_set(const std::string& key, const ValueType& value)
    {
        TrieNode<uint>* node = _getNode(key, true);
        if (node->is_value == false)
        {
            node->value = _add(key, value);
            node->is_value = true;
            return true;
        }
        return false;
    }

    template <typename ValueType>
    void TrieMap<ValueType>::set(const std::string& key, const ValueType& value)
    {
        TrieNode<uint>* node = _getNode(key, true);
        node->value = _add(key, value);
        node->is_value = true;
    }

    template <typename ValueType>
    bool TrieMap<ValueType>::contains(const std::string& key) const
    {
        TrieNode<uint>* node = _getNode(key, false);
        if (node == nullptr)
        {
            return false;
        }
        return node->is_value;
    }

    template <typename ValueType>
    ValueType* TrieMap<ValueType>::find(const std::string& key)
    {
        TrieNode<uint>* node = _getNode(key, false);
        if (node == nullptr)
        {
            return nullptr;
        }
        return &_keyValues[node->value].second;
    }

    template <typename ValueType>
    bool TrieMap<ValueType>::remove(const std::string& key)
    {
        TrieNode<uint>* node = _getNode(key, false);
        if (node == nullptr || node->is_value == false)
        {
            return false;
        }

        node->is_value = false;
        _remove(node->value);
        return true;
    }

    template <typename ValueType>
    TrieNode<uint>* TrieMap<ValueType>::_getNode(const std::string& key, bool create_nodes) const
    {
        unsigned size = key.size();
        assert(size > 0);

        TrieNode<uint>* current = _root;
        for (unsigned i = 0; (current != nullptr) && (i < size); ++i)
        {
            current = (create_nodes)
                ? current->add_node(key.at(i))
                : current->get_node(key.at(i));
        }
        return current;
    }

    template <typename ValueType>
    uint TrieMap<ValueType>::_add(const std::string& key, const ValueType& value)
    {
        uint index;
        if (_openIndices.isEmpty() == false)
        {
            index = _openIndices.back();
            _openIndices.popBack();
            _keyValues[index] = KeyValuePair(key, value);
        }
        else
        {
            index = _keyValues.size();
            _keyValues.pushBack(KeyValuePair(key, value));
        }
        return index;
    }

    template <typename ValueType>
    ValueType& TrieMap<ValueType>::_remove(uint index)
    {
        _openIndices.pushBack(index);
        return _keyValues[index].second;
    }

    template <typename ValueType>
    typename TrieMap<ValueType>::KeyList TrieMap<ValueType>::getKeys(void) const
    {
        KeyList keys;
        for (uint i = 0; i < _keyValues.size(); ++i)
        {
            if (_openIndices.find(i) == _openIndices.size())
            {
                keys.pushBack(_keyValues[i].first);
            }
        }
        return keys;
    }

    template <typename ValueType>
    typename TrieMap<ValueType>::ValueList TrieMap<ValueType>::getValues(void) const
    {
        ValueList values;
        for (uint i = 0; i < _keyValues.size(); ++i)
        {
            if (_openIndices.find(i) == _openIndices.size())
            {
                values.pushBack(_keyValues[i].second);
            }
        }
        return values;
    }
} // namespace Engine

#endif // TrieMap_HPP
#ifndef QUEUE_H
#define QUEUE_H

namespace Engine
{
    template <typename ValueType>
    class Queue
    {
    public:
        Queue(unsigned int capacity);
        ~Queue(void);

        bool        pushBack(const ValueType& value);
        bool        pushFront(const ValueType& item);

        void        popBack(void);
        void        popFront(void);

        ValueType&  back(void);
        ValueType&  front(void);

        unsigned    size(void);
        unsigned    capacity(void);
        bool        isFull(void);
        bool        isEmpty(void);

    private:
        unsigned    _head = 0;
        unsigned    _tail = 0;
        unsigned    _size = 0;
        unsigned    _capacity;
        ValueType*  _buffer;
    };

    //////////////////////////////////////////////////////////
    // Constuctor
    //////////////////////////////////////////////////////////

    template <typename ValueType>
    Queue<ValueType>::Queue(unsigned capacity)
        : _capacity(capacity)
        , _buffer(new ValueType[capacity])
    {
    }

    template <typename ValueType>
    Queue<ValueType>::~Queue(void)
    {
        delete _buffer;
    }

    template <typename ValueType>
    bool Queue<ValueType>::pushBack(const ValueType& value)
    {
        if (_size < _capacity)
        {
            _buffer[_tail] = value;
            _tail = (_tail + 1) % _capacity;
            _size++;

            return true;
        }
        return false;
    }

    template <typename ValueType>
    bool Queue<ValueType>::pushFront(const ValueType& value)
    {
        if (_size < capacity)
        {
            _head = (_head == 0) ? _capacity - 1 : _head - 1;
            _size++;

            _buffer[_head] = value;

            return true;
        }
        return false;
    }

    template <typename ValueType>
    void Queue<ValueType>::popBack(void)
    {
        assert(_size != 0);
        --_size;
        _tail = (_tail ==) ? _capacity - 1 : _tail - 1;
    }

    template <typename ValueType>
    void Queue<ValueType>::popFront(void)
    {
        assert(_size != 0);
        --_size;
        _head = (_head + 1) % _capacity;

    }

    template<typename ValueType>
    ValueType& Queue<ValueType>::back(void)
    {
        return _buffer[_tail - 1];
    }

    template<typename ValueType>
    ValueType& Queue<ValueType>::front(void)
    {
        return _buffer[_head];
    }

    template<typename ValueType>
    unsigned Queue<ValueType>::size(void)
    {
        return _size;
    }

    template<typename ValueType>
    unsigned Queue<ValueType>::capacity(void)
    {
        return _capacity();
    }

    template<typename ValueType>
    bool Queue<ValueType>::isFull(void)
    {
        return (_size == _capacity) ? true : false;
    }

    template<typename ValueType>
    bool Queue<ValueType>::isEmpty(void)
    {
        return (_size == 0);
    }
}

#endif // QUEUE_H
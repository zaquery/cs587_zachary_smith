#ifndef COMPONENT_H
#define COMPONENT_H

namespace Engine
{
    class Actor;
    class JsonElement;

    class Component
    {
    public:
        Actor* owner(void)
        {
            return _owner;
        }
        
        void setOwner(Actor* owner)
        {
            _owner = owner;
        }

        virtual bool        init(JsonElement* data) = 0;
        virtual Component*  clone(void) = 0;    // Return clone of component - no reference to actor
    protected:
        Actor* _owner;
    };
}

#endif // COMPONENT_H
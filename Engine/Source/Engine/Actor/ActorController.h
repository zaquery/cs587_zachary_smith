#ifndef ACTOR_CONTROLLER_H
#define ACTOR_CONTROLLER_H

#include "ITickable.h"
#include "Component.h"

namespace Engine
{
	class ActorController
		: public Component
		, public ITickable
	{

	};
}

#endif // ACTOR_CONTROLLER_H
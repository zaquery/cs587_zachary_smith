#ifndef ACTOR_PHYSICS_H
#define ACTOR_PHYSICS_H

#include "Physics\AABB.h"
#include "Component.h"
#include "Event\IEventHandler.h"

namespace Engine
{
    class Physics;

    struct ActorPhysics
        : public Component
        , public IEventHandler
    {
        AABB boundingBox;

        float mass;
        float invMass;

        float density;
        float restitution;

        Vec2f force;
        Vec2f velocity;

        virtual void on(Event* event) = 0;

        virtual bool registerWithPhysicsSystem(Physics& physics);
        virtual bool unregisterWithPhysicsSystem(Physics& physics);
    };
}

#endif // ACTOR_PHSYICS_H
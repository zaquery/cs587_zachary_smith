#include "Transform.h"

#include "ActorEvent.h"
#include "Event\EventDispatcher.h"

namespace Engine
{
    void Transform::sendAABBChangeEvent(void)
    {
        ActorEvent* event = new ActorEvent(ActorEvent::TransformChange, owner());
        EventDispatcher::instance().triggerEvent(event);
    }
}
#include "ActorPhysics.h"
#include "System\Physics.h"

namespace Engine
{
    bool ActorPhysics::registerWithPhysicsSystem(Physics& physics)
    {
        physics.registerPhysics(this);
        return true;
    }

    bool ActorPhysics::unregisterWithPhysicsSystem(Physics& physics)
    {
        physics.unregisterPhysics(this);
        return true;
    }
}
#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <cmath>

#include "Math/Vec2.h"
#include "Component.h"
#include "Json.h"

namespace Engine
{
    class Transform
        : public Component
    {
    public:
        Vec2f position;
        float rotation;

        Transform(Vec2f position = Vec2f(0, 0), float rotation = 0)
            : position(position)
            , rotation(rotation)
        { 
            /* EMPTY */
        }

        virtual bool init(JsonElement* data)
        {
            position.x = data->asObject()->find("x")->asPrimitive()->asFloat();
            position.y = data->asObject()->find("y")->asPrimitive()->asFloat();
            rotation = data->asObject()->find("rotation")->asPrimitive()->asFloat();

            return true;
        }

        virtual Component* clone(void)
        {
            return new Transform(*this);
        }

		void rotate(float value)
		{
			rotation += value;

            sendAABBChangeEvent();
		}

		void move(float x, float y)
		{
			position += Vec2f(x, y);
            sendAABBChangeEvent();
		}

		void move(Vec2f delta)
		{
			position += delta;
            sendAABBChangeEvent();
		}

        Vec2f direction(void)
        {
            return Vec2f(position.x * std::cos(rotation), position.y * std::sin(rotation));
        }

        void sendAABBChangeEvent(void);
    };
}
#endif // TRANSFORM_H
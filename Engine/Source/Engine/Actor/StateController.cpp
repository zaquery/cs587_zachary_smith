#include "StateController.h"
#include <cassert>

namespace Engine
{
    bool StateController::init(JsonElement* data)
    {
        JsonElement* stateData = data->asObject()->find("states");
        assert(stateData != nullptr);
        StateMachine::init(stateData);
        transition(data->asObject()->find("start_state")->asPrimitive()->asString());
        return true;
    }

    void StateController::on(Event* event)
    {
        assert(_currentState != nullptr && event != nullptr);
        _currentState->handleInput(_owner, static_cast<InputEvent*>(event));
    }

    void StateController::tick(float deltaTime)
    {
        assert(_currentState != nullptr);
        _currentState->update(_owner, deltaTime);
    }
}
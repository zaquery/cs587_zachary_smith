#include "ActorFactory.h"

#include <iostream>

namespace Engine
{
    ActorFactory::ActorFactory(void)
    {
        _lastIdAssigned = 0;
		addComponentFactory("SpriteDrawable", createComponent<SpriteDrawable>);
        addComponentFactory("SimplePhysics", createComponent<SimplePhysics>);
    }

    Actor* ActorFactory::createActor(const std::string& actorRes, bool isPrototype)
    {
        JsonElement* data = ResourceManager::instance().getJsonResource("actor\\" + actorRes);

        Actor* actor = createActor(data->asObject(), isPrototype);

        delete data;
        return actor;
    }

    Actor* ActorFactory::createActor(JsonObject* data, bool isPrototype)
    {
        assert(data != nullptr);

        Actor* actor = new Actor();
        actor->_id = _getNewActorId();

        Transform* transform = new Transform();

        JsonElement* compData = data->asObject()->find("transform");
        if (compData != nullptr)
        {
            transform->init(compData->asObject());
        }
        actor->_setTransform(transform);

        compData = data->asObject()->find("drawable");
        if (compData != nullptr)
        {
            IDrawable* drawable = static_cast<IDrawable*>(_createComponent(compData));
            assert(drawable != nullptr);
            actor->_setDrawable(drawable);
        }

        compData = data->asObject()->find("physics");
        if (compData != nullptr)
        {
            ActorPhysics* physics = static_cast<ActorPhysics*>(_createComponent(compData));
            assert(physics != nullptr);
            actor->_setPhysics(physics);
        }

		compData = data->asObject()->find("controller");
		if (compData != nullptr)
		{
			ActorController* controller = static_cast<ActorController*>(_createComponent(compData));
			assert(controller != nullptr);
			actor->_setController(controller);
		}

        if (isPrototype == false && actor->physics() != nullptr)
        {
            ActorEvent* event = new ActorEvent(ActorEvent::ColliderChange, actor);
            EventDispatcher::instance().queueEvent(event);
        }

		return actor;
    }

    Actor* ActorFactory::clone(Actor* original, Transform* initialTransform)
    {
        assert(initialTransform != nullptr);

        Actor* clone = new Actor();
        clone->_id = _getNewActorId();

        clone->_setTransform(initialTransform);

        if (original->drawable() != nullptr)
        {
            clone->_setDrawable(static_cast<IDrawable*>(original->drawable()->clone()));
        }

        if (original->physics() != nullptr)
        {
            clone->_setPhysics(static_cast<ActorPhysics*>(original->physics()->clone()));
        }

		if (original->controller() != nullptr)
		{
			clone->_setController(static_cast<ActorController*>(original->controller()->clone()));
		}

        ActorEvent* event = new ActorEvent(ActorEvent::ColliderChange, clone);
        EventDispatcher::instance().queueEvent(event);
        return clone;
    }

	void ActorFactory::addComponentFactory(const std::string& name, ComponentFactory factory)
	{
		_componentFactories.set(name, factory);
	}

    Component* ActorFactory::_createComponent(JsonElement* data)
    {
        std::string compName = data->asObject()->find("name")->asPrimitive()->asString();
        auto factoryMethod = _componentFactories.find(compName);
        if (factoryMethod == nullptr)
        {
            return nullptr;
        }

        Component* component = (*factoryMethod)();
        if (component->init(data) == false)
        {
			delete component;
            return nullptr;
        }
        return component;
    }
}
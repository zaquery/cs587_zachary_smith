#ifndef COMPONENT_FACTORIES_H
#define COMPONENT_FACTORIES_H

#include "Component.h"
#include "Transform.h"
#include "SpriteDrawable.h"

namespace Engine
{
    Component* CreateTransform()
    {
        return new Transform();
    }

    Component* CreateSpriteDrawable()
    {
        return new SpriteDrawable();
    }
}

#endif // COMPONENT_FACTORIES_H
#ifndef ACTOR_FACTORY_H
#define ACTOR_FACTORY_H

#include "Actor.h"
#include "Component.h"
#include "Transform.h"
#include "IDrawable.h"

#include "Collections/TrieMap.h"
#include "Singleton.h"
#include "Json.h"

#include "SpriteDrawable.h"
#include "SimplePhysics.h"
#include "StateController.h"

#include "ActorEvent.h"
#include "Event\EventDispatcher.h"

namespace Engine
{
    typedef Component* (*ComponentFactory) (void);
    typedef TrieMap<ComponentFactory> ComponentFactoryMap;

    class ActorFactory
        : public Singleton<ActorFactory>
    {
    public:
        ActorFactory(void);
        ActorPtr    createActor(const std::string& actorRes, bool isPrototype = false);
        ActorPtr    createActor(JsonObject* data, bool isPrototype = false);
        ActorPtr	clone(Actor* original, Transform* initialTransform);
		void	    addComponentFactory(const std::string& name, ComponentFactory factory);

    private:
        Component*  _createComponent(JsonElement* data);
        ActorId     _getNewActorId(void) { return ++_lastIdAssigned; }

        ActorId             _lastIdAssigned;
        ComponentFactoryMap _componentFactories;
    };

	template <typename ValueType>
	Component* createComponent(void)
	{
		return new ValueType();
	}
}

#endif // ACTOR_FACTORY_H
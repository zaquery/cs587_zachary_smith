#ifndef STATE_CONTROLLER_H
#define STATE_CONTROLLER_H

#include "ActorController.h"
#include "Event\IEventHandler.h"
#include "State\StateMachine.h"
#include "Input.h"

namespace Engine
{
    class StateController
        : public ActorController
        , public IEventHandler
        , public StateMachine
    {
    public :
        virtual bool init(JsonElement* data);

        void on(Event* event);
        void tick(float deltaTime);
    };
}

#endif // STATE_CONTROLLER_H
#ifndef ACTOR_H
#define ACTOR_H

#include "Json.h"

#include "Transform.h"
#include "Component.h"
#include "IDrawable.h"
#include "ActorPhysics.h"
#include "ActorController.h"

namespace Engine
{
    class ActorFactory;
    class ActorManager;

    typedef unsigned	ActorId;
    typedef Actor*		ActorPtr;

    class Actor
    {
    public:
        ActorId id(void)
        {
            return _id;
        }

        Transform* transform(void)
        {
            return _transform;
        }

        IDrawable* drawable(void)
        {
            return _drawable;
        }

        ActorPhysics* physics(void)
        {
            return _physics;
        }

        ActorController* controller(void)
        {
            return _controller;
        }

    private:
        // Constructor
        Actor(void);
        ~Actor(void);

        void _setTransform(Transform* transform);
        void _setDrawable(IDrawable* drawable);
        void _setPhysics(ActorPhysics* phyics);
        void _setController(ActorController* controller);

        ActorId				_id;
        Transform*			_transform;
        IDrawable*			_drawable;
        ActorPhysics*       _physics;
        ActorController*	_controller;

        friend ActorFactory;
        friend ActorManager;
    };
}

#endif // ACTOR_H
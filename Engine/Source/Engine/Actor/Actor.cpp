#include "Actor.h"

namespace Engine
{
	Actor::Actor(void)
		: _transform(nullptr)
		, _drawable(nullptr)
		, _controller(nullptr)
        , _physics(nullptr)
	{
		/* EMPTY */
	}

	Actor::~Actor(void)
	{
		delete _transform;
		delete _drawable;
        delete _physics;
		delete _controller;
	}

	void Actor::_setDrawable(IDrawable* drawable)
	{
		if (_drawable != nullptr)
		{
			delete _drawable;
		}
		_drawable = drawable;
		_drawable->setOwner(this);
	}

	void Actor::_setController(ActorController* controller)
	{
		if (_controller != nullptr)
		{
			delete _controller;
		}
		_controller = controller;
		_controller->setOwner(this);
	}

    void Actor::_setPhysics(ActorPhysics* physics)
    {
        if (_physics != nullptr)
        {
            delete _physics;
        }
        _physics = physics;
        _physics->setOwner(this);
    }

    void Actor::_setTransform(Transform* transform)
    {
        if (_transform != nullptr)
        {
            delete _transform;
        }
        _transform = transform;
        _transform->setOwner(this);
    }
}
#ifndef SPRITE_DRAWABLE_H
#define SPRITE_DRAWABLE_H

#include <SFML\Graphics.hpp>

#include "Actor.h"
#include "Transform.h"
#include "IDrawable.h"
#include "Json.h"
#include "Manager\ResourceManager.h"

namespace Engine
{
    class SpriteDrawable
        : public IDrawable
    {
    public:
        virtual bool        init(JsonElement* data);
        virtual Component*  clone(void);
        virtual void        draw(sf::RenderTarget* target, float deltaTime);

        virtual AABB        getAABB(void);
    private: 
        sf::Sprite _sprite;
    };
}

#endif // SPRITE_DRAWABLE_H
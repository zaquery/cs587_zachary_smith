#ifndef IDRAWABLE_H
#define IDRAWABLE_H

#include "SFML/Graphics.hpp"
#include "Component.h"
#include "Physics\AABB.h"

namespace Engine
{
    class IDrawable
        : public Component
    {
    public:
        virtual void draw(sf::RenderTarget* target, float deltaTime) = 0;
        virtual AABB getAABB(void) = 0;
    };
}

#endif // IDRAWABLE_H
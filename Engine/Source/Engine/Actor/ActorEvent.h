#ifndef ACTOR_EVENT_H
#define ACTOR_EVENT_H

#include "Actor.h"
#include "Event\Event.h"

namespace Engine
{
    struct ActorEvent
        : public Event
    {
        static const std::string ColliderChange;
        static const std::string TransformChange;

        ActorPtr actor;

        ActorEvent(const std::string& type, ActorPtr actor);
    };
}

#endif // ACTOR_EVENT_H
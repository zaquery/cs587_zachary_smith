#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "Actor.h"
#include "ITickable.h"
#include "Collections\TrieMap.h"

namespace Engine
{
    class State
    {
        virtual void update(ActorPtr actor);
    };

    class StateMachine
    {
    public:
        State&  currentState(void);
        
    private:
        State*          _currentState;
        TrieMap<State>  _states;
    };
}

#endif // STATE_MACHINE_H
#ifndef SIMPLE_PHYSICS_H
#define SIMPLE_PHYSICS_H

#include "Json.h"
#include "ActorPhysics.h"

namespace Engine
{
    struct SimplePhysics
        : public ActorPhysics
    {
        void on(Event* event);
        bool init(JsonElement* data);

        Component* clone(void);
    };
}

#endif // SIMPLE_PHYSICS_H
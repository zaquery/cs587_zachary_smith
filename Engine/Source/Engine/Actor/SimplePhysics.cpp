#include "SimplePhysics.h"

namespace Engine
{
    void SimplePhysics::on(Event* event)
    {

    }

    bool SimplePhysics::init(JsonElement* data)
    {
        mass = data->asObject()->find("mass")->asPrimitive()->asFloat();
        invMass = (mass != 0) ? 1 / mass : 0.0f;

        density = data->asObject()->find("density")->asPrimitive()->asFloat();
        restitution = data->asObject()->find("restitution")->asPrimitive()->asFloat();

        return true;
    }

    Component* SimplePhysics::clone(void)
    {
        SimplePhysics* clone = new SimplePhysics(*this);
        return clone;
    }
}
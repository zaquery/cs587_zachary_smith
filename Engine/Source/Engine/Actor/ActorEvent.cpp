#include "ActorEvent.h"

namespace Engine
{
    const std::string ActorEvent::ColliderChange = "colliderChange";
    const std::string ActorEvent::TransformChange = "transform_change";

    ActorEvent::ActorEvent(const std::string& type, ActorPtr actor)
        : Event(type)
        , actor(actor)
    {

    }
}
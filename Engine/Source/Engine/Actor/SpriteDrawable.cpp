#include "SpriteDrawable.h"

namespace Engine
{
    bool SpriteDrawable::init(JsonElement* data)
    {
        std::string texturePath = data->asObject()->find("texture")->asPrimitive()->asString();

        sf::Texture* texture = ResourceManager::instance().getTextureResource(texturePath);
        if (texture == nullptr)
        {
            return false;
        }

        _sprite.setTexture(*texture);

        auto bounds = _sprite.getLocalBounds();
        _sprite.setOrigin(bounds.width/2, bounds.height/2);

        return true;
    }

    void SpriteDrawable::draw(sf::RenderTarget* target, float deltaTime)
    {
        Transform* transform = owner()->transform();
        _sprite.setPosition(transform->position.x, transform->position.y);
        _sprite.setRotation(transform->rotation);

        target->draw(_sprite);
    }

    Component* SpriteDrawable::clone(void)
    {
        SpriteDrawable* clone = new SpriteDrawable();
        clone->_sprite.setTexture(*_sprite.getTexture());
        clone->_sprite.setOrigin(_sprite.getOrigin());
        return clone;
    }

    AABB SpriteDrawable::getAABB(void)
    {
        Transform* transform = owner()->transform();
        /*_sprite.setPosition(transform->position.x, transform->position.y);
        _sprite.setRotation(transform->rotation);
*/
        float radians = transform->rotation * Util::DEG_TO_RAD;
        auto bounds = _sprite.getLocalBounds();
        Vec2f dims;
        dims.x = bounds.width * abs(cos(radians)) + bounds.height * abs(sin(radians));
        dims.y = bounds.width * abs(sin(radians)) + bounds.height * abs(cos(radians));

        return AABB(transform->position - dims / 2.0f, dims);
    }
}
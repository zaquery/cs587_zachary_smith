#include "Game.h"

namespace Engine
{
    Renderer& Game::renderer(void)
    {
        return _renderer;
    }

    bool Game::init(void)
    {
        ResourceManager::instance().init("resources/");

        JsonElement* data = JsonParser::parseFromFile("config.json");
        assert(data != nullptr);

        _title = data->asObject()->find("title")->asPrimitive()->asString();
        uint height = data->asObject()->find("window_height")->asPrimitive()->asInt();
        uint width = data->asObject()->find("window_width")->asPrimitive()->asInt();

        _window.create(sf::VideoMode(width, height), _title);

        JsonPrimitive* sceneName = data->asObject()->find("first_scene")->asPrimitive();
        assert(sceneName != nullptr);
        SceneManager::instance().loadScene(sceneName->asString());

        delete data;
        return true;
    }

    void Game::run(void)
    {
        _isRunning = true;
        sf::Clock clock;

        while (_isRunning)
        {
            sf::Event event;
            while (_window.pollEvent(event))
            {

                if (InputEvent::isKeyboardInput(event) || InputEvent::isMouseInput(event))
                {
                    InputManager::instance().triggerEvent(event);
                }

                if (event.type == sf::Event::Closed)
                {
                    _window.close();
                    _isRunning = false;
                }
            }

            EventDispatcher::instance().triggerAll();

            float time = clock.restart().asSeconds();
            SceneManager::instance().current().advance(time);
            _renderer.advance(time);
        }
    }

    bool Game::shutdown(void)
    {
        return true;
    }

    sf::RenderWindow* Game::window(void)
    {
        return &_window;
    }
}
#include "Manager\ResourceManager.h"

namespace Engine
{
	void ResourceManager::init(const std::string& basePath)
	{
		if (_initialized)
		{
			return;
		}

		_basePath = basePath;
		_initialized = true;
	}

	void ResourceManager::deinit(void)
	{
		auto textures = _textureCache.getValues();
		for (uint index = 0; index < textures.size(); index++)
		{
			delete textures[index];
		}
		_textureCache = TrieMap<Texture*>();

		auto json = _jsonCache.getValues();
		for (uint index = 0; index < json.size(); index++)
		{
			delete json[index];
		}
		_jsonCache = TrieMap<JsonElement*>();

		_initialized = false;
	}

	JsonElement* ResourceManager::getJsonResource(const std::string& filename, bool cache)
	{
		assert(_initialized);

		JsonElement* value = nullptr;
		if (_jsonCache.contains(filename) == false)
		{
			value = JsonParser::parseFromFile(_basePath + "json/" + filename);
			if (value != nullptr && cache)
			{
				_jsonCache[filename] = value;
			}
		}
		else
		{
			value = _jsonCache[filename];
		}

		return value;
	}

	Texture* ResourceManager::getTextureResource(const std::string& filename, bool cache)
	{
		assert(_initialized);

		Texture* texture = nullptr;
		if (_textureCache.contains(filename) == false)
		{
			texture = new Texture();
			if (texture->loadFromFile(_basePath + "texture/" + filename) == false)
			{
				delete texture;
				texture = nullptr;
			}

			if (cache)
			{
				_textureCache[filename] = texture;
			}
		}
		else
		{
			texture = _textureCache[filename];
		}

		return texture;
	}
}
#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include <string>
#include "Scene\Scene.h"
#include "Singleton.h"

namespace Engine
{
    class SceneManager
        : public Singleton<SceneManager>
    {
    public:
        Scene&  current(void);
        bool    loadScene(const std::string& sceneRes);

    private:
        Scene* _current;
    };
}

#endif // SCENE_MANAGER_H
#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <SFML\Graphics.hpp>

#include "Json.h"
#include "Collections\TrieMap.h"
#include "Singleton.h"

namespace Engine
{
	typedef sf::GlResource Resource;
	typedef sf::Texture Texture;

	class ResourceManager
        : public Singleton<ResourceManager>
	{
	public:
		void init(const std::string& basePath);
		void deinit(void);

		JsonElement*	getJsonResource(const std::string& filename, bool cache = true);
		Texture*		getTextureResource(const std::string& filename, bool cache = true);

		void			removeJsonResource(const std::string& path);
		void			removeTextureResource(const std::string& path);

	private:
		bool					_initialized = false;
		std::string				_basePath;
		TrieMap<JsonElement*>	_jsonCache;
		TrieMap<Texture*>		_textureCache;
	};
}

#endif // RESOURCE_MANAGER_H
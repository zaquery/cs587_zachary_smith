#include "SceneManager.h"

namespace Engine
{

    Scene& SceneManager::current(void)
    {
        return *_current;
    }

    bool SceneManager::loadScene(const std::string& sceneRes)
    {
        if (_current != nullptr)
        {
            _current->onFinish();
            delete _current;
        }

        JsonElement* sceneData = ResourceManager::instance().getJsonResource("scene/" + sceneRes);
        assert(sceneData != nullptr);
        _current = new Scene();
        _current->init(sceneData);

        return true;
    }


}
#ifndef RENDERER_H
#define RENDERER_H

#include "SFML\Graphics.hpp"

#include "Scene\Scene.h"
#include "Manager\SceneManager.h"
namespace Engine
{
    class Renderer
    {
    public:
        void advance(float dt);
    };
}

#endif // RENDERER_H
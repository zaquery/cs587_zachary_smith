#include "Renderer.h"
#include "Game.h"
#include "DrawUtils.h"

#include <iostream>

namespace Engine
{
    void Renderer::advance(float dt)
    {
        sf::RenderWindow* window = Game::instance().window();
        window->clear();
        
        auto actorList = SceneManager::instance().current().actors();
        for (uint index = 0; index < actorList.size(); index++)
        {
            IDrawable* currentDrawable = actorList[index]->drawable();
            if (currentDrawable != nullptr)
            {
                currentDrawable->draw(window, dt);
            }

#if 0
            ActorPhysics* physics = actorList[index]->physics();
            if (physics != nullptr)
            {
                //std::cout << "Draw " << std::endl;
                drawAABB(physics->boundingBox, window);
            }
#endif
        }

        window->display();
    }
}
#ifndef PHYSICS_H
#define PHYSICS_H

#include "Actor\Actor.h"
#include "ITickable.h"
#include "Event\IEventHandler.h"

#include "Physics\Manifold.h"
#include "Physics\QuadTree.h"

#include "Actor\ActorEvent.h"
#include "Event\EventDispatcher.h"

namespace Engine
{
    class Physics
        : public ITickable
        , public IEventHandler
    {
    public:
        static Vec2f gravity;

        bool init(JsonElement* data);
        bool cleanup(void);

        bool registerPhysics(ActorPhysics* physics);
        bool unregisterPhysics(ActorPhysics* phsyics);

        void tick(float deltaTime);
        void on(Event* event);

        void updateActor(Actor* actor, AABB& aabb);

    private:
        DynamicArray<ActorPhysics*> _actorPhysics;
        QuadTree    _tree;

        float       _sinkingCorrPercent;
        float       _sinkingCorrSlop;

        Manifold    getCollision(ActorPtr actorOne, ActorPtr actorTwo);
        void        handleCollision(Manifold& manifold);
    };
}

#endif // PHYSICS_H
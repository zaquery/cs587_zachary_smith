#include "Physics.h"
#include <iostream>

namespace Engine
{
    Vec2f Physics::gravity = Vec2f(0.0f, 0.0f);

    bool Physics::init(JsonElement* data)
    {
        if (data == nullptr)
        {
            return false;
        }

        JsonObject* gravityData = data->asObject()->find("gravity")->asObject();
        gravity.x = gravityData->find("x")->asPrimitive()->asFloat();
        gravity.y = gravityData->find("y")->asPrimitive()->asFloat();

        _sinkingCorrPercent = data->asObject()->find("sinking_corr_percent")->asPrimitive()->asFloat();
        _sinkingCorrSlop = data->asObject()->find("sinking_corr_slop")->asPrimitive()->asFloat();

        JsonObject* gameRegion = data->asObject()->find("game_region")->asObject();
        float width = gameRegion->find("width")->asPrimitive()->asFloat();
        float height = gameRegion->find("height")->asPrimitive()->asFloat();

        Vec2f bounds = Vec2f(width, height);
        _tree = QuadTree(AABB(Vec2f(0, 0), bounds));

        EventDispatcher::instance().registerHandler(ActorEvent::ColliderChange, this);
        EventDispatcher::instance().registerHandler(ActorEvent::TransformChange, this);

        return true;
    }

    bool Physics::cleanup(void)
    {
        EventDispatcher::instance().unregisterHandler(ActorEvent::ColliderChange, this);
        EventDispatcher::instance().unregisterHandler(ActorEvent::TransformChange, this);
        return true;
    }

    bool Physics::registerPhysics(ActorPhysics* physics)
    {
        if (_tree.addActor(physics->owner()) == true)
        {
            _actorPhysics.pushBack(physics);
            return true;
        }
        return false;
    }

    bool Physics::unregisterPhysics(ActorPhysics* physics)
    {
        if (_tree.removeActor(physics->owner()) == true)
        {
            int index = _actorPhysics.find(physics);
            assert(index != -1);
            _actorPhysics[index] = _actorPhysics.back();
            _actorPhysics.popBack();
            return true;
        }

        return false;
    }

    void Physics::tick(float deltaTime)
    {
        for (unsigned int index = 0; index < _actorPhysics.size(); ++index)
        {
            DynamicArray<Actor*> collisions;
            _tree.query(collisions, _actorPhysics[index]->boundingBox);

            for (unsigned int index2 = 0; index2 < collisions.size(); ++index2)
            {
                // handle collisions
                if (_actorPhysics[index]->owner()->id() != collisions[index2]->id())
                {
                    std::cout << "COLLISION" << std::endl;
                    handleCollision(getCollision(_actorPhysics[index]->owner(), collisions[index2]));
                }
            }

            _actorPhysics[index]->velocity += (gravity * deltaTime);
            _actorPhysics[index]->owner()->transform()->move(_actorPhysics[index]->velocity * deltaTime);
        }
    }

    void Physics::on(Event* event)
    {
        if (event->eventType == ActorEvent::TransformChange || event->eventType == ActorEvent::ColliderChange)
        {
            ActorEvent* actorEvent = static_cast<ActorEvent*>(event);
            if (actorEvent->actor->physics() != nullptr)
            {
                actorEvent->actor->physics()->boundingBox = actorEvent->actor->drawable()->getAABB();
                _tree.refreshActor(actorEvent->actor);
            }
        }
    }

    void Physics::updateActor(Actor* actor, AABB& aabb)
    {
        actor->physics()->boundingBox = aabb;
        _tree.refreshActor(actor);
    }


    Manifold Physics::getCollision(Actor* actorOne, Actor* actorTwo)
    {
        AABB intersectionArea;
        if (actorOne->physics()->boundingBox.intersects(actorTwo->physics()->boundingBox, intersectionArea) == false)
        {
            assert(false); // This should have been eliminated in the broad phase detection
            return Manifold(actorOne, actorTwo, 0.0f, Vec2f(0.0f, 0.0f));
        }

        Vec2f normal = (actorTwo->transform()->position - actorOne->transform()->position) * -1;
        Vec2f dims = intersectionArea.bounds();

        if (dims.y < dims.x)
        {
            normal.x = 0.0f;
        }
        else
        {
            normal.y = 0.0f;
        }

        normal.normalize();

        return Manifold(actorOne, actorTwo, Util::min(dims.x, dims.y), normal);
    }

    void Physics::handleCollision(Manifold& manifold)
    {
        ActorPhysics* physicsOne = manifold.actorTwo->physics();
        ActorPhysics* physicsTwo = manifold.actorOne->physics();

        Vec2f relativeVelocity = physicsTwo->velocity - physicsOne->velocity;
        float normalVelocity = relativeVelocity.dot(manifold.normal);

        // objects are moving away from each other already
        if (normalVelocity > 0)
        {
            return;
        }

        float invSum = physicsOne->invMass + physicsTwo->invMass;
        float restitution = Util::min(physicsOne->restitution, physicsTwo->restitution);
        float impulseMagnitude = -(1 + restitution) * normalVelocity / (invSum);

        Vec2f impulse = impulseMagnitude * manifold.normal;
        physicsOne->velocity -= physicsOne->invMass * impulse;
        physicsTwo->velocity += physicsTwo->invMass * impulse;

        Vec2f sinkingCorrection = Util::max(manifold.ingress, 0.0f) / (invSum) * _sinkingCorrPercent * manifold.normal;

        manifold.actorOne->transform()->move(-1 * physicsOne->invMass * sinkingCorrection);
        manifold.actorTwo->transform()->move(physicsTwo->invMass * sinkingCorrection);
    }
}
#include "QuadTree.h"

namespace Engine
{
    QuadTree::QuadTree(const AABB& startRegion)
    {
        _root = new QuadTreeNode(startRegion, nullptr);
    }

    QuadTree::QuadTree(const QuadTree& other)
    {
        AABB startRegion = other._root->getRegion();

        _root = new QuadTreeNode(startRegion, nullptr);

        for (unsigned int iter = 0; iter < other._actors.size(); ++iter)
        {
            addActor(other._actors[iter]);
        }
    }

    QuadTree& QuadTree::operator=(const QuadTree& other)
    {
        if (this != &other)
        {
            AABB startRegion = other._root->getRegion();

            _root = new QuadTreeNode(startRegion, nullptr);

            for (unsigned int iter = 0; iter < other._actors.size(); ++iter)
            {
                addActor(other._actors[iter]);
            }
        }

        return *this;
    }

    QuadTree::~QuadTree()
    {
        delete _root;
        clearOccupants();
    }

    bool QuadTree::addActor(Actor* actor)
    {
        int index = findOccupant(actor);

        if (index == -1)
        {
            QuadTreeOccupant* occupant = createOccupant(actor);
            _root->insert(occupant);

            _actors.pushBack(actor);
            _occupants.pushBack(occupant);

            return true;
        }

        return false;
    }

    bool QuadTree::removeActor(Actor* actor)
    {
        int index = findOccupant(actor);

        if (index != -1)
        {
            _root->remove(_occupants[index], true);
            _actors.removeAt(index);

            delete _occupants[index];
            _occupants.removeAt(index);

            return true;
        }

        return false;
    }

    bool QuadTree::refreshActor(Actor* actor)
    {
        int index = findOccupant(actor);

        if (index != -1)
        {
            _root->refresh(_occupants[index]);

            return true;
        }

        return false;
    }

    void QuadTree::clearTree(const AABB& newStartRegion)
    {
        delete _root;
        _root = new QuadTreeNode(newStartRegion, nullptr, this);

        clearOccupants();
    }

    void QuadTree::query(DynamicArray<Actor*>& queryResult, const AABB& queryRegion)
    {
        DynamicArray<QuadTreeOccupant*> queryResultsFromTree;

        // query the tree
        _root->Query(queryResultsFromTree, queryRegion);

        for (unsigned int iter = 0; iter < queryResultsFromTree.size(); iter++)
        {
            queryResult.pushBack(queryResultsFromTree[iter]->actor);
        }
    }

    int QuadTree::findOccupant(Actor* actor)
    {
        for (uint index = 0; index < _actors.size(); index++)
        {
            if (actor->id() == _actors[index]->id())
            {
                return index;
            }
        }
        return -1;
    }

    QuadTreeOccupant* QuadTree::createOccupant(Actor* actor)
    {
        if (_actors.contains(actor) == false)
        {
            return new QuadTreeOccupant(actor, nullptr);
        }

        return nullptr;
    }

    void QuadTree::clearOccupants()
    {
        for (unsigned int iter = 0; iter < _occupants.size(); iter++)
        {
            delete _occupants[iter];
        }

        _occupants.resize(0);
        _actors.resize(0);
    }
}
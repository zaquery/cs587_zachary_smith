#ifndef MANIFOLD_H
#define MANIFOLD_H

#include "Actor\Actor.h"

namespace Engine
{
    struct Manifold
    {
        ActorPtr    actorOne;
        ActorPtr    actorTwo;
        float       ingress;
        Vec2f       normal;

        Manifold(ActorPtr actorOne, ActorPtr actorTwo, float ingress, Vec2f normal)
            : actorOne(actorOne)
            , actorTwo(actorTwo)
            , ingress(ingress)
            , normal(normal)
        {
            /* EMPTY */
        }
    };
}

#endif // MANIFOLD_H

#ifndef QUAD_TREE_NODE_H
#define QUAD_TREE_NODE_H

#include "Actor\Actor.h"
#include "Collections\DynamicArray.h"

#include "QuadTreeOccupant.h"

namespace Engine
{
    class QuadTree;

    class QuadTreeNode
    {  
    public:
        const static unsigned MAX_OCCUPANTS = 5;

        QuadTreeNode(const AABB& region, QuadTreeNode* parent = nullptr, QuadTree* owner = nullptr);
        ~QuadTreeNode(void);

        void insert(QuadTreeOccupant* occupant);
        void remove(QuadTreeOccupant* occupant, bool clean);
        void refresh(QuadTreeOccupant* occupant);
        void Query(DynamicArray<QuadTreeOccupant*>& queryResult, const AABB& queryRegion);

        const AABB& getRegion(void) const;

    private:
        void                _subdivide(void);
        void                _cleanUpwards(void);
        void                _removeChildren(void);
            
        bool                _isEmptyLeaf(void);
            
        DynamicArray<AABB>  _getChildrenBoundingBoxes(void);
        void                _getAllOccupants(DynamicArray<QuadTreeOccupant*>& occupants);
        QuadTreeNode*       _getDestinationNode(QuadTreeOccupant* occupant);
        AABB                _region;
        QuadTreeNode*       _parent;
        DynamicArray<QuadTreeOccupant*> _occupants;

        QuadTreeNode* _childTL = nullptr;
        QuadTreeNode* _childTR = nullptr;
        QuadTreeNode* _childBL = nullptr;
        QuadTreeNode* _childBR = nullptr;
    };
}

#endif // QUAD_TREE_NODE_H
#ifndef QUAD_TREE_OCCUPANT_H
#define QUAD_TREE_OCCUPANT_H

#include "Actor\Actor.h"

namespace Engine
{
    class QuadTreeNode;

    struct QuadTreeOccupant
    {
        ActorPtr        actor;
        QuadTreeNode*   parent;

        QuadTreeOccupant(ActorPtr actor, QuadTreeNode* parent)
            : actor(actor)
            , parent(parent)
        {
            /* EMPTY */
        }

        AABB getBoundingBox(void)
        {
            return actor->physics()->boundingBox;
        }
    };
}

#endif // QUAD_TREE_OCCUPANT_H
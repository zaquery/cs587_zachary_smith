#ifndef QUAD_TREE_H
#define QUAD_TREE_H

#include "Actor\Actor.h"
#include "Collections\DynamicArray.h"
#include "AABB.h"
#include "QuadTreeOccupant.h"
#include "QuadTreeNode.h"

namespace Engine
{
    class QuadTree
    {
    public: 
        const static unsigned int MAX_DEPTH = 20;

        QuadTree(const AABB& startingRegion = AABB());
        QuadTree(const QuadTree& other);
        ~QuadTree(void);

        QuadTree& operator=(const QuadTree& other);

        bool addActor(ActorPtr actor);
        bool removeActor(ActorPtr actor);
        bool refreshActor(ActorPtr actor);
        void clearTree(const AABB& startingRegion);
        void query(DynamicArray<ActorPtr>& queryResult, const AABB& queryRegion);

    private:
        int                 findOccupant(ActorPtr actor);
        QuadTreeOccupant*   createOccupant(ActorPtr actor);
        void                clearOccupants(void);

        // parallel arrays to store the actors and their corresponding quad occupants
        DynamicArray<Actor*> _actors;
        DynamicArray<QuadTreeOccupant*> _occupants;
        QuadTreeNode* _root;
    };
}

#endif // QUAD_TREE_H
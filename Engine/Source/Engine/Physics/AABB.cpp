#include "AABB.h"

namespace Engine
{
	AABB::AABB(const Vec2f& topLeft, const Vec2f& bottomRight)
		: _upperBound(topLeft)
		, _bounds(bottomRight)
	{
		/* EMPTY */
	}

	Vec2f AABB::center(void) const
	{
        return _upperBound + _bounds / 2.0f;
	}

	Vec2f AABB::bounds(void) const
	{
        return _bounds;
	}

	Vec2f AABB::lowerBound(void) const
	{
        return _upperBound + _bounds;
	}

	Vec2f AABB::upperBound(void) const
	{
        return _upperBound;
	}

	void AABB::setCenter(const Vec2f& newCenter)
	{
		Vec2f delta = newCenter - center();
		move(delta);
	}

	void AABB::move(const Vec2f& delta)
	{
        _upperBound += delta;
	}

	void AABB::setBounds(const Vec2f& newBounds)
	{
        _bounds = newBounds;
	}

    bool AABB::intersects(const AABB& other) const
    {
        AABB intersection;
        return intersects(other, intersection);
    }

	bool AABB::intersects(const AABB& other, AABB& intersection) const
	{
        Vec2f lower = lowerBound();

        // compute min and max of the first AABB on both axes
        float r1MinX = Util::min(lower.x, _upperBound.x);
        float r1MaxX = Util::max(lower.x, _upperBound.x);
        float r1MinY = Util::min(lower.y, _upperBound.y);
        float r1MaxY = Util::max(lower.y, _upperBound.y);

        Vec2f otherLower = other.lowerBound();

        // compute min and max of the second AABB on both axes
        float r2MinX = Util::min(otherLower.x, other._upperBound.x);
        float r2MaxX = Util::max(otherLower.x, other._upperBound.x);
        float r2MinY = Util::min(otherLower.y, other._upperBound.y);
        float r2MaxY = Util::max(otherLower.y, other._upperBound.y);

        // compute the intersection boundaries
        float interLeft = Util::max(r1MinX, r2MinX);
        float interTop = Util::max(r1MinY, r2MinY);
        float interRight = Util::min(r1MaxX, r2MaxX);
        float interBottom = Util::min(r1MaxY, r2MaxY);

        // If the intersection is valid (positive non zero area), then there is an intersection
        if ((interLeft < interRight) && (interTop < interBottom))
        {
            Vec2f topLeft = Vec2f(interLeft, interTop);
            Vec2f dims = Vec2f(interRight - interLeft, interBottom - interTop);

            intersection = AABB(topLeft, dims);
            return true;
        }
        else
        {
            intersection = AABB(Vec2f(0, 0), Vec2f(0, 0));
            return false;
        }
	}

	bool AABB::contains(const AABB& other) const
	{
        return ((_upperBound.x <= other._upperBound.x) &&
                (_upperBound.y <= other._upperBound.y) &&
                (_upperBound.x + _bounds.x) >= (other._upperBound.x + other._bounds.x) &&
                (_upperBound.y + _bounds.y) >= (other._upperBound.y + other._bounds.y));
	}
}
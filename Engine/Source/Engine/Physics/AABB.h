#ifndef AABB_H
#define AABB_H

#include "Util.h"
#include "Math\Vec2.h"

namespace Engine
{
	class AABB
	{
	public:
        //AABB(void);
		AABB(const Vec2f& upperBound = Vec2f(), const Vec2f& dims = Vec2f());

		Vec2f center(void) const;
		Vec2f bounds(void) const;
		Vec2f lowerBound(void) const;
		Vec2f upperBound(void) const;

		void move(const Vec2f& delta);
		void setCenter(const Vec2f& newCenter);
		void setBounds(const Vec2f& newBounds);

		bool intersects(const AABB& other) const;
        bool intersects(const AABB& other, AABB& intersectionArea) const;
		bool contains(const AABB& other) const;

	private:
		Vec2f _upperBound;
		Vec2f _bounds;
	};
}

#endif // AABB_H
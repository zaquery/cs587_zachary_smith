#include "QuadTreeNode.h"

namespace Engine
{
    QuadTreeNode::QuadTreeNode(const AABB &region, QuadTreeNode* parent, QuadTree* pContainer)
        : _region(region), _parent(parent)
    {
    }

    QuadTreeNode::~QuadTreeNode()
    {
        _removeChildren();
    }

    void QuadTreeNode::insert(QuadTreeOccupant* occupant)
    {
        // if the quad doesn't contain the occupant, then put it at root level
        if (_region.contains(occupant->getBoundingBox()) == false)
        {
            if (_parent == nullptr)
            {
                _occupants.pushBack(occupant);
            }

            return;
        }

        // if there is room at the current node
        if (_childTL == nullptr && (_occupants.size() + 1) < MAX_OCCUPANTS)
        {
            _occupants.pushBack(occupant);
        }
        else
        {
            if (_childTL == nullptr)
            {
                _subdivide();
            }

            // Find out into which child the occupant should go into
            QuadTreeNode* dest = _getDestinationNode(occupant);

            if (dest == this)
            {
                _occupants.pushBack(occupant);
            }
            else
            {
                dest->insert(occupant);
            }
        }
    }

    void QuadTreeNode::remove(QuadTreeOccupant* occupant, bool clean)
    {
        if (occupant->parent != nullptr)
        {
            if (occupant->parent == this)
            {
                _occupants.removeAt(_occupants.find(occupant));
                if (clean)
                {
                    _cleanUpwards();
                }
            }
            else
            {
                occupant->parent->remove(occupant, clean);
            }
        }
    }

    void QuadTreeNode::refresh(QuadTreeOccupant* occupant)
    {
        // Is the node still contained in the same parent?
        if (_region.contains(occupant->getBoundingBox()))
        {
            // check if the object moved into any of the children
            if (_childTL != nullptr)
            {
                QuadTreeNode* dest = _getDestinationNode(occupant);

                if (occupant->parent != dest)
                {
                    QuadTreeNode* formerParent = occupant->parent;
                    remove(occupant, false);
                    dest->insert(occupant);

                    // clean up
                    formerParent->_cleanUpwards();
                }
            }
        }
        else
        {
            if (_parent != nullptr)
            {
                _parent->refresh(occupant);
            }
        }

    }

    void QuadTreeNode::Query(DynamicArray<QuadTreeOccupant*>& queryResult, const AABB& queryRegion)
    {
        // See if this region is visible
        if (queryRegion.contains(_region))
        {
            _getAllOccupants(queryResult);
        }
        else if (queryRegion.intersects(_region))
        {
            for (unsigned int iter = 0; iter < _occupants.size(); iter++)
            {
                if (queryRegion.intersects(_occupants[iter]->getBoundingBox()))
                {
                    queryResult.pushBack(_occupants[iter]);
                }
            }

            if (_childTL != nullptr)
            {
                _childTL->Query(queryResult, queryRegion);
                _childTR->Query(queryResult, queryRegion);
                _childBL->Query(queryResult, queryRegion);
                _childBR->Query(queryResult, queryRegion);
            }
        }
    }

    const AABB& QuadTreeNode::getRegion() const
    {
        return _region;
    }

    void QuadTreeNode::_subdivide()
    {
        DynamicArray<AABB> boundingBoxes = _getChildrenBoundingBoxes();

        _childTL = new QuadTreeNode(boundingBoxes[0], this);
        _childTR = new QuadTreeNode(boundingBoxes[1], this);
        _childBL = new QuadTreeNode(boundingBoxes[2], this);
        _childBR = new QuadTreeNode(boundingBoxes[3], this);

        // If objects are completely contained by child quads, bump them down
        for (unsigned index = _occupants.size() - 1; index >= 0; index--)
        {
            QuadTreeNode* dest = _getDestinationNode(_occupants[index]);

            if (dest != this)
            {
                dest->insert(_occupants[index]);
                _occupants.removeAt(index);
            }
        }
    }

    void QuadTreeNode::_cleanUpwards()
    {
        if (_childTL != nullptr)
        {
            if (_childTL->_isEmptyLeaf() && _childTR->_isEmptyLeaf() &&
                _childBL->_isEmptyLeaf() && _childBR->_isEmptyLeaf())
            {
                _removeChildren();
            }
        }

        if (_parent != nullptr && _occupants.size() == 0)
        {
            _parent->_cleanUpwards();
        }
    }

    void QuadTreeNode::_removeChildren()
    {
        if (_childTL != nullptr)
        {
            delete _childTL;
            delete _childTR;
            delete _childBL;
            delete _childBR;

            _childTL = _childTR = _childBL = _childBR = nullptr;
        }
    }

    bool QuadTreeNode::_isEmptyLeaf()
    {
        return (_occupants.size() == 0);
    }

    DynamicArray<AABB> QuadTreeNode::_getChildrenBoundingBoxes()
    {
        DynamicArray<AABB> boundingBoxes;

        Vec2f halfSize = _region.bounds() / 2.0f;
        Vec2f center = _region.center();

        for (unsigned int x = 0; x < 2; x++)
        {
            for (unsigned int y = 0; y < 2; y++)
            {
                Vec2f lowerBound = _region.lowerBound();
                AABB region = AABB(Vec2f(lowerBound.x + x * halfSize.x, lowerBound.y + y * halfSize.y),
                                   Vec2f(center.x + x * halfSize.x, center.y + y * halfSize.y));

                boundingBoxes.pushBack(region);
            }
        }

        return boundingBoxes;
    }

    void QuadTreeNode::_getAllOccupants(DynamicArray<QuadTreeOccupant*>& occupants)
    {
        for (unsigned int iter = 0; iter < _occupants.size(); iter++)
        {
            occupants.pushBack(_occupants[iter]);
        }

        if (_childTL != nullptr)
        {
            _childTL->_getAllOccupants(occupants);
            _childTR->_getAllOccupants(occupants);
            _childBL->_getAllOccupants(occupants);
            _childBR->_getAllOccupants(occupants);
        }
    }

    QuadTreeNode* QuadTreeNode::_getDestinationNode(QuadTreeOccupant* occupant)
    {
        QuadTreeNode* dest = this;
        AABB boundingBox = occupant->getBoundingBox();

        if (_childTL->_region.contains(boundingBox) == true)
        {
            dest = _childTL;
        }
        else if (_childTR->_region.contains(boundingBox) == true)
        {
            dest = _childTR;
        }
        else if (_childBL->_region.contains(boundingBox) == true)
        {
            dest = _childBL;
        }
        else if (_childBR->_region.contains(boundingBox) == true)
        {
            dest = _childBR;
        }

        return dest;
    }
}
#ifndef DRAW_UTILS_H
#define DRAW_UTILS_H

#include "Physics\AABB.h"

namespace Engine
{
    void drawAABB(AABB& box, sf::RenderWindow* renderTarget)
    {
        sf::RectangleShape outline;

        Vec2f dims = box.bounds();

        outline.setSize(sf::Vector2f(dims.x, dims.y));
        outline.setPosition(box.center().x - dims.x / 2.0f, box.center().y - dims.y / 2.0f);

        outline.setOutlineColor(sf::Color::Red);
        outline.setOutlineThickness(2.0);

        outline.setFillColor(sf::Color::Transparent);
        renderTarget->draw(outline);
    }
}

#endif // DRAW_UTILS_H
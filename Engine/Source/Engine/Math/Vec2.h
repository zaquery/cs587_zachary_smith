#ifndef VEC2_HPP
#define VEC2_HPP

#include <cmath>

namespace Engine
{
    template <typename ValueType>
    class Vec2
    {
    public:
        Vec2(ValueType s = ValueType());
        Vec2(ValueType x, ValueType y);

        Vec2(const Vec2& copy) = default;

        ValueType   length(void) const;
        void        normalize(void);

        ValueType   dot(const Vec2& vec2) const;

        Vec2        operator-(void) const;
        Vec2        operator+(const Vec2& vec2) const;
        Vec2        operator-(const Vec2& vec2) const;
        Vec2        operator*(ValueType s) const;
        Vec2        operator/(ValueType s) const;

        Vec2        operator+=(const Vec2& vec2);
        Vec2        operator-=(const Vec2& vec2);
        Vec2        operator*=(ValueType s);
        Vec2        operator/=(ValueType s);
        
        ValueType x;
        ValueType y;
    };

    //////////////////////////////////////////////////////////
    // Constructors
    //////////////////////////////////////////////////////////

    template<typename ValueType> 
    Vec2<ValueType>::Vec2(ValueType s)
        : x(s)
        , y(s)
    {
        /* EMPTY */
    }

    template<typename ValueType>
    Vec2<ValueType>::Vec2(ValueType x, ValueType y)
        : x(x)
        , y(y)
    {
        /* EMPTY */
    }

    template<typename ValueType>
    ValueType Vec2<ValueType>::length(void) const
    {
        return std::sqrt(x*x + y*y);
    }

    template<typename ValueType>
    void Vec2<ValueType>::normalize(void)
    {
        ValueType len = length();
        if (len != 0)
        {
            x /= len;
            y /= len;
        }
    }
    
    template<typename ValueType>
    ValueType Vec2<ValueType>::dot(const Vec2<ValueType>& vec) const
    {
        return x*vec.x + y*vec.y;
    }

    template<typename ValueType>
    Vec2<ValueType> Vec2<ValueType>::operator-(void) const
    {
        return this * -1;
    }

    template<typename ValueType>
    Vec2<ValueType> Vec2<ValueType>::operator+(const Vec2& rhs) const
    {
        return Vec2(x + rhs.x, y + rhs.y);
    }

    template<typename ValueType>
	Vec2<ValueType> Vec2<ValueType>::operator-(const Vec2 &rhs) const
    {
        return Vec2(x - rhs.x, y - rhs.y);
    }

    template<typename ValueType>
    Vec2<ValueType> Vec2<ValueType>::operator*(ValueType rhs) const
    {
        return Vec2(x*rhs, y*rhs);
    }

    template<typename ValueType>
    Vec2<ValueType> Vec2<ValueType>::operator/(ValueType rhs) const
    {
        return Vec2(x/rhs, y/rhs);
    }
    
    template<typename ValueType> 
    Vec2<ValueType> Vec2<ValueType>::operator+=(const Vec2& rhs)
    {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    template<typename ValueType>
    Vec2<ValueType> Vec2<ValueType>::operator-=(const Vec2& rhs)
    {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    template<typename ValueType> 
    Vec2<ValueType> Vec2<ValueType>::operator*=(ValueType s)
    {
        x *= s;
        y *= s;
        return *this;
    }

    template <typename ValueType>
    Vec2<ValueType> Vec2<ValueType>::operator/=(ValueType s)
    {
        x /= s;
        y /= s;
        return *this;
    }

    template <typename ValueType>
    Vec2<ValueType> operator*(const ValueType s, const Vec2<ValueType>& vec)
    {
        return vec * s;
    }

    typedef Vec2<int>       Vec2i;
    typedef Vec2<unsigned>  Vec2u;
    typedef Vec2<float>     Vec2f;
}
#endif // VEC2_HPP
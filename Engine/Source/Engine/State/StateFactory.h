#ifndef STATE_FACTORY_H
#define STATE_FACTORY_H

#include "Singleton.h"
#include "State.h"
#include "Collections\TrieMap.h"

namespace Engine
{
    class StateFactory
        : public Singleton<StateFactory>
    {
    public:
        typedef State* (*StateCreator)(void);
        typedef TrieMap<StateCreator> StateCreatorMap;

        State*  createState(JsonElement* data);
        void    registerCreator(const std::string& type, StateCreator creator);

    private:
        StateCreatorMap _stateCreators;
    };
}

#endif // STATE_FACTORY_H
#include "StateFactory.h"

namespace Engine
{
    State* StateFactory::createState(JsonElement* data)
    {
        std::string type = data->asObject()->find("name")->asPrimitive()->asString();
        StateCreator* creator = _stateCreators.find(type);
        assert(creator != nullptr);
        State* state = (*creator)();
        state->type = type;
        state->init(data);
        return state;
    }

    void StateFactory::registerCreator(const std::string& type, StateCreator creator)
    {
        _stateCreators.set(type, creator);
    }
}
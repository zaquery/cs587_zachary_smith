#ifndef STATE_H
#define STATE_H

#include <string>
#include "Input.h"
#include "Json.h"

namespace Engine
{
    class Actor;

    struct State
    {
        std::string type;

        State(const std::string& type)
            : type(type)
        {
        }

        virtual void    init(JsonElement* data) = 0;
        virtual State*  clone(void) = 0;

        virtual void    handleInput(Actor* actor, InputEvent* event) = 0;
        virtual void    update(Actor* actor, float deltaTime) = 0;
        virtual void    onEnter(void)  {};
        virtual void    onExit(void)   {};
    };
}

#endif // STATE_H

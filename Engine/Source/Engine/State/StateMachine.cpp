#include "StateMachine.h"

namespace Engine
{
    StateMachine::~StateMachine(void)
    {
        for (unsigned index = 0; index < _states.size(); index++)
        {
            delete _states[index];
        }
    }

    void StateMachine::transition(const std::string& type)
    {
        State* nextState = nullptr;
        for (unsigned index = 0; index < _states.size(); index++)
        {
            if (_states[index]->type == type)
            {
                nextState = _states[index];
                break;
            }
        }

        assert(nextState != nullptr);

        if (_currentState != nullptr)
        {
            _currentState->onExit();
        }

        _currentState = nextState;
        _currentState->onEnter();
    }

    void StateMachine::init(JsonElement* data)
    {
        JsonArray* states = data->asArray();
        for (unsigned index = 0; index < states->count(); index++)
        {
            _states.pushBack(StateFactory::instance().createState(states->get(index)));
        }
    }

    void StateMachine::clone(DynamicArray<State*>& states, State* currentState)
    {
        for (unsigned index = 0; index < states.size(); index++)
        {
            _states.pushBack(_states[index]->clone());
        }

        transition(currentState->type);
    }
}
#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "Json.h"
#include "State.h"
#include "StateFactory.h"

namespace Engine
{
    class Actor;

    class StateMachine
    {
    public:
        ~StateMachine(void);

        void transition(const std::string& nextState);
        void init(JsonElement* data);
        void clone(DynamicArray<State*>& states, State* currentState);

    protected:
        DynamicArray<State*>    _states;
        State*                  _currentState;
    };
}

#endif // STATE_MACHINE_H
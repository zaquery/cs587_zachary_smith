#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "SFML\Window.hpp"

#include "InputEvent.h"
#include "Event\EventDispatcher.h"

namespace Engine
{
    class InputManager
        : public EventDispatcher
    {
    public:
        static  InputManager& instance(void);
        void    triggerEvent(sf::Event& event);

        static bool isKeyPressed(Keyboard::KeyCode key);
    };
}

#endif // INPUT_MANAGER_H
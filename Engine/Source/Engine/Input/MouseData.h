#ifndef MOUSE_DATA_H
#define MOUSE_DATA_H

#include "SFML\Window.hpp"

namespace Engine
{
    namespace Mouse
    {
        enum Button : int;
        enum MouseActionType : int;
    }

    struct MouseData
    {
        Mouse::MouseActionType actionType;
        Mouse::Button button;

        int x;
        int y;
        int delta;

        MouseData(const sf::Event& event)
        {
            actionType = static_cast<Mouse::MouseActionType>(event.type);

            switch (event.type)
            {
            case sf::Event::MouseWheelMoved:
                delta = event.mouseWheel.delta;
                x = event.mouseWheel.x;
                y = event.mouseWheel.y;
                break;

            case sf::Event::MouseButtonPressed:
            case sf::Event::MouseButtonReleased:
                x = event.mouseButton.x;
                y = event.mouseButton.y;
                button = static_cast<Mouse::Button>(event.mouseButton.button);
                break;
            case sf::Event::MouseMoved:
                x = event.mouseMove.x;
                y = event.mouseMove.y;
                break;

            default:
                break;
            }
        }
    };

    namespace Mouse
    {
        enum Button : int
        {
            Left,
            Right,
            Middle,
            XButton1,
            XButton2,

            ButtonCount // Keep last -- total number of mouse buttons
        };

        enum MouseActionType : int
        {
            MouseWheelMove = 7,
            MouseButtonPressed,
            MouseButtonReleased,
            MouseMoved,
            MouseEntered,
            MouseLeft
        };
    }
}

#endif // MOUSE_DATA_H
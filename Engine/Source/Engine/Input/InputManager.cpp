#include "InputManager.h"

namespace Engine
{
    InputManager& InputManager::instance(void)
    {
        static InputManager* manager = new InputManager();
        return *manager;
    }

    void InputManager::triggerEvent(sf::Event& event)
    {
        Event* inputEvent = new InputEvent(event);
        EventDispatcher::triggerEvent(inputEvent);
    }

    bool InputManager::isKeyPressed(Keyboard::KeyCode key)
    {
        sf::Keyboard::Key keyValue = static_cast<sf::Keyboard::Key>(key);
        return sf::Keyboard::isKeyPressed(keyValue);
    }
}
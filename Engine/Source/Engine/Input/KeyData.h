#ifndef KEY_DATA_H
#define KEY_DATA_H

#include "SFML\Window.hpp"

namespace Engine
{
    namespace Keyboard
    {
        enum KeyboardActionType : int;
        enum KeyCode : int;
    }

    struct KeyData
    {
        Keyboard::KeyboardActionType actionType;
        Keyboard::KeyCode key;
        bool alt;
        bool control;
        bool shift;

        KeyData(const sf::Event& event)
        {
            actionType = static_cast<Keyboard::KeyboardActionType>(event.type);

            switch (event.type)
            {
            case sf::Event::KeyPressed:
            case sf::Event::KeyReleased:
                key = static_cast<Keyboard::KeyCode>(event.key.code);
                alt = event.key.alt;
                control = event.key.alt;
                shift = event.key.shift;
                break;
            case sf::Event::TextEntered:
                key = static_cast<Keyboard::KeyCode>(event.text.unicode);
                break;
            default:
                break;
            }
        }
    };

    namespace Keyboard
    {
        enum KeyboardActionType : int
        {

            KeyPressed = 5,
            KeyReleased,
            TextEntered
        };

        enum KeyCode : int
        {
            Unknown = -1,
            A = 0,
            B,
            C,
            D,
            E,
            F,
            G,
            H,
            I,
            J,
            K,
            L,
            M,
            N,
            O,
            P,
            Q,
            R,
            S,
            T,
            U,
            V,
            W,
            X,
            Y,
            Z,
            Num0,
            Num1,
            Num2,
            Num3,
            Num4,
            Num5,
            Num6,
            Num7,
            Num8,
            Num9,
            Escape,
            LControl,
            LShift,
            LAlt,
            LSystem,
            RControl,
            RShift,
            RAlt,
            RSystem,
            Menu,
            LBracket,
            RBracket,
            SemiColon,
            Comma,
            Period,
            Quote,
            Slash,
            BackSlash,
            Tilde,
            Equal,
            Dash,
            Space,
            Return,
            BackSpace,
            Tab,
            PageUp,
            PageDown,
            End,
            Home,
            Insert,
            Delete,
            Add,
            Subtract,
            Multiply,
            Divide,
            Left,
            Right,
            Up,
            Down,
            Numpad0,
            Numpad1,
            Numpad2,
            Numpad3,
            Numpad4,
            Numpad5,
            Numpad6,
            Numpad7,
            Numpad8,
            Numpad9,
            F1,
            F2,
            F3,
            F4,
            F5,
            F6,
            F7,
            F8,
            F9,
            F10,
            F11,
            F12,
            F13,
            F14,
            F15,
            Pause,
            KeyCount
        };
    }
}

#endif // KEY_DATA_H
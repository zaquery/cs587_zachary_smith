#include "InputEvent.h"

namespace Engine
{
    std::string const InputEvent::KeyboardEvent = "keyboardEvent";
    std::string const InputEvent::MouseEvent = "mouseEvent";

    InputEvent::InputEvent(const sf::Event& event)
        : Event(isKeyboardInput(event) ? KeyboardEvent : MouseEvent)
    {
        if (isKeyboardInput(event) == true)
        {
            keyData = new KeyData(event);
        }
        else
        {
            mouseData = new MouseData(event);
        }
    }

    InputEvent::~InputEvent(void)
    {
        if (eventType == KeyboardEvent)
        {
            delete keyData;
        }
        else
        {
            delete mouseData;
        }
    }

    bool InputEvent::isKeyboardInput(const sf::Event& event)
    {
        switch (event.type)
        {
        case sf::Event::KeyPressed:
        case sf::Event::KeyReleased:
        case sf::Event::TextEntered:
            return true;
        default:
            return false;
        }
    }
    bool InputEvent::isMouseInput(const sf::Event& event)
    {
        switch (event.type)
        {
        case sf::Event::MouseWheelMoved:
        case sf::Event::MouseButtonPressed:
        case sf::Event::MouseButtonReleased:
        case sf::Event::MouseMoved:
        case sf::Event::MouseEntered:
        case sf::Event::MouseLeft:
            return true;
        default:
            return false;
        }
    }
}
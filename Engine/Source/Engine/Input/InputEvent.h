#ifndef INPUT_EVENT_H
#define INPUT_EVENT_H

#include <string>

#include "KeyData.h"
#include "MouseData.h"
#include "Event\Event.h"

namespace Engine
{
    struct InputEvent
        : public Event
    {
        union
        {
            KeyData*    keyData;
            MouseData*  mouseData;
        };

        InputEvent(const sf::Event& event);
        ~InputEvent(void);

        static bool isKeyboardInput(const sf::Event& event);
        static bool isMouseInput(const sf::Event& event);

        static const std::string KeyboardEvent;
        static const std::string MouseEvent;
    };
}

#endif // INPUT_EVENT_H